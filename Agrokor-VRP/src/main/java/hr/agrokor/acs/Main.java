package hr.agrokor.acs;

import hr.agrokor.acs.model.ACSModel;
import hr.agrokor.acs.model.VRPModel;
import hr.agrokor.cluster.Cluster;
import hr.agrokor.cluster.KMeansPPAlgorithm;
import hr.agrokor.data.DataManipulation;
import hr.agrokor.data.help.SolutionPersist;
import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;
import hr.agrokor.nn.VRPNearestNeighbour1;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {

	private static Map<String, Distance> distMat = DataManipulation
			.getDistanceMatrix(Paths.get("./data/distanceMatrix.mdat"));
	private static List<Location> locations = DataManipulation
			.getLocations(Paths.get("./data/locations.mdat"));

	private static List<Vehicle> vehicles = DataManipulation
			.getVehicleTypes(Paths.get("./data/vehicles.mdat"));
	private static List<Cluster<Location>> clusters = KMeansPPAlgorithm
			.getClusters( new ArrayList<>(locations.subList(1, locations.size())), 1);

	private static int startTime;
	private static int maxDrivingTime = (int) 10.45 * 3600;
	public static List<Integer> bestSol = new ArrayList<Integer>();
	public static void main(String... args) {

		startTime = 6*3600;
		List<Integer> initSolution = extract1(SolutionPersist.loadSolution());
		//List<Integer> initSolution = extract(VRPNearestNeighbour1.solve(locations, distMat, vehicles, startTime));

		List<List<Integer>> clusterSolutions = new ArrayList<>();
		List<List<Location>> locData = new ArrayList<List<Location>>();

		for (Cluster<Location> cluster : clusters) {
			List<Location> clusterList = cluster.getMembers();
			clusterList.add(0, locations.get(0));
			clusterSolutions.add(extract(VRPNearestNeighbour1.solve(
					clusterList, distMat, vehicles, startTime)));
			locData.add(clusterList);
		}

		double ro = 0.56;
		double alpha = 7.1;
		double beta = 4.52;

		List<ACSModel> modelList = new ArrayList<>();

		bestSol = initSolution;


		int id = 0;
		modelList.clear();

		for (List<Integer> sol : clusterSolutions) {
			modelList.add(new VRPModel(distMat, locData.get(id++), vehicles,
					bestSol, ro, alpha, beta, startTime, 1, maxDrivingTime));
		}

		List<AntColonySystem> acsList = new ArrayList<AntColonySystem>();
		List<List<Integer>> finalSolutions = new ArrayList<>();

		for (ACSModel mdlList : modelList) {
			acsList.add(new AntColonySystem(mdlList, 25, 100));
		}

		for (AntColonySystem acs : acsList) {
			finalSolutions.add(acs.solve());

		}

		for (List<Integer> sol : finalSolutions) {

			//showSolution(sol);

		}
		System.out.println("Najbolje rjesenje");
		Main.showSolution(bestSol, Paths.get("./data/complete.in"));
		for ( int i = 0;  i < finalSolutions.size(); ++i ) {
			clusterSolutions.set(i, finalSolutions.get(i));
			initSolution = finalSolutions.get(i);
		}


	}

	private static List<Integer> extract1(List<List<Integer>> initSolution) {
		List<Integer> solution = new ArrayList<>();
		for (List<Integer> list : initSolution) {
			list.remove(list.size() - 1); // maknem zadnju lokaciju koja je
			// warehouse
			for (Integer integer : list) {
				solution.add(integer);
			}
		}
		solution.add(0); // dodam warehouse
		return solution;
	}

	public static boolean checkFeasibility(List<List<Integer>> solution) {
		for (List<Integer> list : solution) {
			List<Location> locs = getLocs(list);
			int weight = sumWeight(locs);
			int volume = sumVolume(locs);
			System.out.println("Težina: " + weight + ", volumen: " + volume);
		}

		for (List<Integer> list : solution) {
			int currentTime = startTime;
			int drivingTime = 0;
			List<Location> locs = getLocs(list);

			for (int i = 0; i < locs.size() - 1; i++) {
				Location loc1 = locs.get(i);
				Location loc2 = locs.get(i + 1);
				String key = VRPModel.getKey(loc1, loc2);

				int duration = distMat.get(key).getDuration();

				int arrivalTime = Math.max(currentTime + duration,
						loc2.getWindowStart());
				if (arrivalTime > loc2.getWindowEnd()) {
					return false;
				}

				int delta = arrivalTime - currentTime + loc2.getDeliveryTime();
				currentTime += delta;
			}
			if (currentTime - startTime > maxDrivingTime) {
				System.out.println("NOT FEASIBLE " + currentTime / 3600.0);
				return false;
			}

		}

		return true;
	}

	private static int sumVolume(List<Location> locs) {
		int sum = 0;
		for (Location locationData : locs) {
			sum += locationData.getCargoVolume();
		}
		return sum;
	}

	private static int sumWeight(List<Location> locs) {
		int sum = 0;
		for (Location locationData : locs) {
			sum += locationData.getCargoWeight();
		}
		return sum;
	}

	private static List<Location> getLocs(List<Integer> list) {
		List<Location> locs = new ArrayList<>();
		for (Integer integer : list) {
			locs.add(locations.get(integer));
		}
		return locs;
	}

	public static int getSolutionCost(List<Integer> moves) {

		int timeCost = 0;
		int oneRouteCost = 0;

		int currentTime = 6 * 3600;

		for (int i = 0; i < moves.size() - 1; ++i) {
			Location loc1 = locations.get(moves.get(i));
			Location loc2 = locations.get(moves.get(i + 1));
			String key = VRPModel.getKey(loc1, loc2);
			int duration = distMat.get(key).getDuration();
			int arrivalTime = Math.max(currentTime + duration, loc2.getWindowStart());
			int delta = loc2.getDeliveryTime() + arrivalTime - currentTime;
			currentTime += delta;
			oneRouteCost += delta;
			if (loc2.getName().equals("SKLADIŠTE")) {
				timeCost += oneRouteCost;
				oneRouteCost = 0;
				currentTime = 6 * 3600;
			}

		}

		return timeCost;
	}

	public static void showSolution(List<Integer> finalSolution, Path file) {
		List<List<Integer>> solution = transformSolution(finalSolution);
		int distance = calculateFullCost(solution);
		int timeCost = getSolutionCost(finalSolution);

		System.out.println("Valjanost: " + checkFeasibility(solution));

		System.out.println("Metraža: " + distance/1000.0);

		System.out.println("Sekundaža: " + timeCost/3600.0);
		PrintStream stdout = System.out;
		try {

			System.setOut(new PrintStream(Files.newOutputStream(file, StandardOpenOption.TRUNCATE_EXISTING)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (List<Integer> list : solution) {
			for (Integer integer : list) {
				System.out.print(integer + " ");
			}
			System.out.print(calculateDistance(list) + " " );
			System.out.print(getSolutionCost(list) +  " ");
			List<Location> locs = getLocs(list);
			System.out.print(sumWeight(locs) + " ");
			System.out.print(sumVolume(locs) + " ");
			System.out.print(startTime);
			System.out.println();
		}
		System.setOut(stdout);
	}

	public static int calculateFullCost(List<List<Integer>> solution) {
		int sum = 0;
		for (List<Integer> list : solution) {
			sum += calculateDistance(list);
		}
		return sum;
	}

	public static int calculateDistance(List<Integer> list) {
		int sum = 0;
		for (int i = 0; i < list.size() - 1; i++) {
			Location loc1 = locations.get(list.get(i));
			Location loc2 = locations.get(list.get(i + 1));
			sum += distMat.get(VRPModel.getKey(loc1, loc2)).getDistance();
		}
		return sum;
	}

	public static List<List<Integer>> transformSolution(
			List<Integer> finalSolution) {
		List<List<Integer>> solution = new ArrayList<>();

		List<Integer> route = new ArrayList<>();
		int countWarehouse = 0;
		for (Integer integer : finalSolution) {
			route.add(integer);
			if (integer == 0) {
				countWarehouse++;
			}
			if (countWarehouse == 2) {
				countWarehouse = 1;
				solution.add(route);
				route = new ArrayList<>();
				route.add(0);
			}
		}
		return solution;
	}

	public static List<Integer> extract(List<List<Location>> initSolution) {
		List<Integer> solution = new ArrayList<>();
		for (List<Location> list : initSolution) {
			list.remove(list.size() - 1); // maknem zadnju lokaciju koja je
			// warehouse
			for (Location loc : list) {
				solution.add(loc.getID());
			}
		}
		solution.add(0); // dodam warehouse
		return solution;
	}

	public static List<Integer> extractFromCluster(
			List<Cluster<Location>> clusters, Location warehouse) {

		List<List<Location>> lists = new ArrayList<>();
		for (Cluster<Location> cluster : clusters) {
			lists.add(cluster.getMembers());
		}

		for (List<Location> list : lists) {
			list.add(0, warehouse);
			list.add(warehouse);
		}

		return extract(lists);
	}
}