package hr.agrokor.acs;

import hr.agrokor.acs.model.ACSModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Represents an ant colony.
 * 
 * It creates {@link Ant}s, sends them on their life of search and danger and
 * expects them to come up with the results. The whole search space/matrix is
 * updated with pheromones and another iteration of {@link Ant}s is sent to
 * improve the current solution.
 * 
 * <br>
 * <br>
 * More can be learned from the method documentation.
 * 
 * @author vjeran
 * 
 */
public class AntColonySystem {

	/** Number of ants. */
	private int numOfAnts;
	/** Pheromone evaporation parameter. */
	private double ro;
	/** Number of search iterations. */
	private int numOfIter;
	/** Model by which the search is guided. */
	private ACSModel model;
	/** Matrix of the pheromones between the pair transitions. */
	private double[][] pheromoneMatrix;
	/** Service used to parallelize the ants. */
	private ExecutorService pool;

	/**
	 * Constructs the {@link AntColonySystem} from the given parameters.
	 * 
	 * @param model
	 *            by which the search is guided
	 * @param numOfAnts
	 *            number of ants
	 * @param numOfIter
	 *            number of search iterations
	 */
	public AntColonySystem(ACSModel model, int numOfAnts, int numOfIter) {
		this.model = model;
		this.numOfAnts = numOfAnts;
		this.numOfIter = numOfIter;
		this.ro = model.ro();
		// pheromone matrix is initialized so that every location has its
		// pairing one
		this.pheromoneMatrix = new double[model.getPheromoneMatrixDimension()][model
		                                                                       .getPheromoneMatrixDimension()];
		this.pool = Executors.newFixedThreadPool(numOfAnts);
		initPheromoneMatrix(model.tauZero());
	}

	/**
	 * Starts the process of optimization.
	 * 
	 * @return list of moves which start the optimization
	 */
	public List<Integer> solve() {
		int counter = 0;
		List<Integer> tmpSolution = model.getInitSolution();
		List<Integer> bestSolution = tmpSolution;

		System.out.println("Kolonija ima "
				+ model.getSolutionCost(bestSolution) / 3600.0);

		while (counter < numOfIter) {
			ArrayList<Ant> ants = new ArrayList<>();

			for (int i = 0; i < numOfAnts; ++i) {
				Ant ant = new Ant(model.copy(), pheromoneMatrix);
				ants.add(ant);
			}

			// blocks this main thread and after the ants finish
			// this thread is unblocked.
			try {
				pool.invokeAll(ants);
			} catch (InterruptedException e) {
				System.err.println("Ants failed and program too. ABORTING...");
				e.printStackTrace();
				System.exit(-1);
			}

			double minCost = Double.MAX_VALUE;

			for (int i = 0; i < numOfAnts; ++i) {

				int tmpCost = model.getSolutionCost((ants.get(i).getMoves()));

				if (tmpCost < minCost) {
					minCost = tmpCost;
					tmpSolution = ants.get(i).getMoves();
				}

			}

			globalPheromoneUpdate(model.getSolutionCost(tmpSolution),
					tmpSolution);
			globalPheromoneUpdate(model.getSolutionCost(bestSolution) / 25.0,
					bestSolution);



			if (model.getSolutionCost(tmpSolution) < model
					.getSolutionCost(Main.bestSol)) {
				Main.bestSol = tmpSolution;
			}
			counter++;
		}

		pool.shutdown();
		if (model.getSolutionCost(bestSolution) < model
				.getSolutionCost(Main.bestSol)) {
			Main.bestSol = bestSolution;
		}


		return bestSolution;

	}

	/**
	 * Updates the pheromone matrix by increasing the pheromone count on the
	 * given path of the given cost. This way the ants of the next iteration are
	 * slowly converging.
	 * 
	 * @param cost
	 *            of the given path
	 * @param path
	 *            of the solution
	 */
	public void globalPheromoneUpdate(double cost, List<Integer> path) {

		int numOfVisits = path.size();

		for (int i = 0; i < numOfVisits - 1; ++i) {
			pheromoneMatrix[path.get(i)][path.get(i + 1)] *= (1 - ro);

			pheromoneMatrix[path.get(i)][path.get(i + 1)] += ro / cost;
		}
		pheromoneMatrix[0][path.get(numOfVisits - 1)] *= 1 - ro;

		pheromoneMatrix[0][path.get(numOfVisits - 1)] += ro / cost;
	}

	/**
	 * Initializes pheromone matrix with the given value.
	 * 
	 * @param tau0
	 *            starting pheromone deposit.
	 */
	private void initPheromoneMatrix(double tau0) {

		int n = pheromoneMatrix.length;

		for (int i = 0; i < n - 1; ++i) {
			for (int j = i; j < n; ++j) {
				pheromoneMatrix[i][j] = tau0;
				pheromoneMatrix[j][i] = pheromoneMatrix[i][j];
			}
		}

	}

}