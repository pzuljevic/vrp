package hr.agrokor.acs.model;

import hr.agrokor.acs.Ant;
import hr.agrokor.acs.AntColonySystem;

import java.util.List;

/**
 * Represents an {@link AntColonySystem} model which allows the colony system to
 * simulate its behavior. Concrete method implementation is given to the user of
 * the interface.
 * 
 * 
 * @author vc46789
 * @see Ant
 * @see AntColonySystem
 */
public interface ACSModel {

	/**
	 * Gets the pheromone matrix dimension. Dimension is equal to the number of
	 * vertices in the graph which is explored by the {@link AntColonySystem}.
	 * 
	 * @return dimension of the pheromone matrix
	 */
	int getPheromoneMatrixDimension();

	/**
	 * Eta is a parameter which takes part in the calculation of the probability
	 * of the transition to the next vertex.
	 * 
	 * @param i
	 *            index of the current vertex
	 * @param j
	 *            index of the potential next vertex
	 * @return weighted probability component
	 */
	double eta(int i, int j);

	/**
	 * Gets the value of the pheromone evaporation parameter.
	 * 
	 * @return pheromone evaporation parameter
	 */
	double ro();

	/**
	 * Gets the initial value of the pheromone matrix fields.
	 * 
	 * @return initial transition probability
	 */
	double tauZero();

	/**
	 * Gets the index of the next vertex. This function should calculate the
	 * next vertex using the weighted probability.
	 * 
	 * @param pheromoneMatrix
	 *            matrix of the pheromone deposits
	 * @return next move
	 */
	int getNextMove(double[][] pheromoneMatrix);

	/**
	 * Returns {@code true} if {@link Ant} has any transitions to do,
	 * {@code false} otherwise.
	 * 
	 * @return {@code true} if {@link Ant} has any transitions to do,
	 *         {@code false} otherwise.
	 */
	boolean canWalk();

	/**
	 * Inserts the non-visited vertices in the solution path.
	 * 
	 * @param moves
	 *            solution path to which the remaining vertices are inserted
	 */
	void insertNonVisited(List<Integer> moves);

	/**
	 * Improves the given solution path.
	 * 
	 * @param moves
	 *            final solution path given by the {@link Ant}
	 */
	void improveMoves(List<Integer> moves);

	/**
	 * Gets the starting position of the {@link Ant}.
	 * 
	 * @return starting vertex of the {@link Ant}
	 */
	int getStartPosition();

	/**
	 * Gets the solution cost of the given {@code moves}. This cost is used by
	 * the AntColonySystem to differentiate the good and bad paths which
	 * {@link Ant}s have constructed.
	 * 
	 * @param moves
	 * @return solution cost
	 */
	int getSolutionCost(List<Integer> moves);

	/**
	 * Gets the initial starting solution which colony uses to initialize its
	 * pheromone matrix.
	 * 
	 * @return starting sequence of moves which {@link Ant}s can take
	 */
	List<Integer> getInitSolution();

	/**
	 * Gives a full copy of the model without any resource sharing.
	 * 
	 * @return full copy
	 */
	ACSModel copy();

}
