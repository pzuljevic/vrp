package hr.agrokor.acs.model;

import hr.agrokor.acs.AntColonySystem;
import hr.agrokor.data.DataManipulation;
import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Implements the {@link ACSModel} interface to offer the
 * {@link AntColonySystem} a chance to solve the vehicle routing problem with
 * time windows.
 * 
 * <br>
 * <br>
 * Plenty of heuristics linked to the problem are used and further can be
 * learned from the method documentation.
 * 
 * @author vjeran
 * 
 */
public class VRPModel implements ACSModel {

	/** This is the matrix of the distance and time used by the algorithm. */
	private Map<String, Distance> distanceTimeMatrix;
	/** List of all locations which take part(or not) in the solution. */
	private static List<Location> locationData = DataManipulation
			.getLocations(Paths.get("./data/locations.mdat"));
	/** These locations take part in the solution. */
	private List<Location> locations;
	/** This list contains vehicle data. */
	private List<Vehicle> vehicleData;
	/**
	 * alpha(pheromone weight), beta(arbitrary cost), ro(evaporation) constants
	 * determined experimentally to speed up the process.
	 */
	private double alpha, beta, ro;
	/** Storage of the initial solution. */
	private List<Integer> initSolution;

	/** Represents the number of vertices in the graph (space of the problem). */
	private int numOfVertices;

	/**
	 * Cost of the initial solution (here concretely, total travelling time of
	 * all vehicles).
	 */
	private int initialCost;

	/** Last visited location. */
	private Location lastVisited;

	/** Capacity used by the current vehicle. */
	private int currentCapacity, currentVolume;
	/** Day-time of the current driving vehicle. */
	private int currentTime;
	/**
	 * Array of numbers which represent the number of vehicles used of certain
	 * type.
	 */
	private Integer[] usedVehicles;
	/** Starting driving time. */
	private int startTime;
	/** Maximum driving time allowed. */
	private int maxDrivingTime;
	/** Warehouse (starting location). */
	private Location warehouse;
	/** Map of unvisited locations. */
	public Map<Integer, Location> unvisitedLocations;
	/** Number of warehouses (if there's more). */
	private int numOfWarehouses;

	/**
	 * Constructs a {@link VRPModel} from the given parameters. More about them
	 * in their tags.
	 * 
	 * @param distanceTimeMatrix
	 *            matrix of distances and time of the pairs of {@link Location}
	 * @param locations
	 *            all of the locations on which the problem will be solved
	 *            (including warehouse on the 0-th position)
	 * @param vehicleData
	 *            list which contains all of the vehicle data
	 * @param initSolution
	 *            initial solution of the problem
	 * @param ro
	 *            evaporation parameter
	 * @param alpha
	 *            pheromone weight parameter
	 * @param beta
	 *            cost weight parameter
	 * @param startTime
	 *            starting driving time of the vehicle
	 * @param numOfWarehouses
	 *            total number of warehouses
	 */
	public VRPModel(Map<String, Distance> distanceTimeMatrix,
			List<Location> locations, List<Vehicle> vehicleData,
			List<Integer> initSolution, double ro, double alpha, double beta,
			int startTime, int numOfWarehouses, int maxDrivingTime) {

		this.ro = ro;
		this.alpha = alpha;
		this.beta = beta;
		this.distanceTimeMatrix = distanceTimeMatrix;
		this.locations = locations;
		this.vehicleData = vehicleData;
		this.usedVehicles = new Integer[vehicleData.size()];
		// initializing to zero vehicle used.
		for (int i = 0; i < usedVehicles.length; ++i) {
			usedVehicles[i] = 0;
		}
		this.currentCapacity = 0;
		this.currentVolume = 0;
		this.currentTime = startTime;
		this.startTime = startTime;
		this.maxDrivingTime = maxDrivingTime;
		this.numOfWarehouses = numOfWarehouses;
		this.unvisitedLocations = new HashMap<>();
		this.warehouse = locations.get(0);
		// initializing unvisited locations
		for (Location loc : locations) {
			if (loc.getID() != warehouse.getID()) {
				this.unvisitedLocations.put(loc.getID(), loc);
			}
		}

		this.initSolution = initSolution;
		this.initialCost = getSolutionCost(initSolution);
		this.lastVisited = this.warehouse;

		this.numOfVertices = locationData.size();
	}

	@Override
	public int getPheromoneMatrixDimension() {
		// ovo bi trebalo biti povezano s klasterima
		// ~broj lokacija u klasteru
		return locationData.size();
	}

	@Override
	public double eta(int i, int j) {
		/*
		 * Equals the time reciprocal value of the time required to travel from
		 * location i to location j.
		 */
		if (i == j) {
			return 0.0;
		}

		Location loc1 = locationData.get(i);
		Location loc2 = locationData.get(j);
		String key = getKey(loc1, loc2);

		int duration = distanceTimeMatrix.get(key).getDuration();

		int deltaTime = Math.max(currentTime + duration, loc2.getWindowStart());
		deltaTime += loc2.getDeliveryTime() - currentTime + 1;

		return 1.0 / deltaTime; // * 1.0 / (loc2.getWindowEnd() - currentTime +
		// 1);
	}

	@Override
	public double tauZero() {
		return 1.0 / (numOfVertices * initialCost);
	}

	/**
	 * Returns the key required to get the value from
	 * {@linkplain #distanceTimeMatrix}.
	 * 
	 * @param loc1
	 *            starting location
	 * @param loc2
	 *            destination
	 * @return
	 */
	public static String getKey(Location loc1, Location loc2) {
		StringBuilder sb = new StringBuilder();

		sb.append(loc1.getGPSLatitude()).append(',')
		.append(loc1.getGPSLongitude()).append(',')
		.append(loc2.getGPSLatitude()).append(',')
		.append(loc2.getGPSLongitude());

		return sb.toString();
	}

	@Override
	public int getNextMove(double[][] pheromoneMatrix) {

		// 0 is depot
		Double randProbability = Math.random();
		double sum = 0;

		Location bestNext = warehouse;

		double total = totalSumOfFactors(pheromoneMatrix);

		// Calculation of the weighted probability
		for (Entry<Integer, Location> pair : unvisitedLocations.entrySet()) {
			if (transitionIsFeasibile(lastVisited, pair.getValue())) {
				sum += transitionProbability(lastVisited.getID(),
						pair.getKey(), pheromoneMatrix, total);
				if (sum >= randProbability) {
					bestNext = pair.getValue();

					break;
				}
			}
		}

		if (bestNext == warehouse) {
			// if we have arrived back at the warehouse current state must be
			// reset
			useOneVehicle();
			currentCapacity = 0;
			currentVolume = 0;
			currentTime = startTime;
		} else {
			// update of the current state
			currentCapacity += bestNext.getCargoWeight();
			currentVolume += bestNext.getCargoVolume();
			currentTime += distanceTimeMatrix
					.get(getKey(lastVisited, bestNext)).getDuration();
			if (currentTime < bestNext.getWindowStart()) {
				currentTime += bestNext.getWindowStart() - currentTime;
			}
			currentTime += bestNext.getDeliveryTime();
			unvisitedLocations.remove(bestNext.getID());
			distanceTimeMatrix.get(getKey(lastVisited, bestNext)).getDuration();
			;
		}
		lastVisited = bestNext;
		return bestNext.getID();
	}

	/**
	 * Checks if there is any vehicle that can carry the current weight and
	 * volume of the cargo.
	 * 
	 * @param i
	 *            index of the vehicle type
	 * @param weight
	 *            of the cargo
	 * @param volume
	 *            of the cargo
	 * @return {@code true} if vehicle can carry, {@code false} otherwise
	 */
	private boolean vehicleCanCarry(int i, int weight, int volume) {

		int vehicleCapacity = vehicleData.get(i).getWeightCap();
		int vehicleVolume = vehicleData.get(i).getVolumeCap();

		return (weight <= vehicleCapacity && volume <= vehicleVolume);
	}

	/**
	 * Checks if there exists a free vehicle.
	 * 
	 * @param i
	 *            index of the vehicle type
	 * @return {@code true} if vehicle exists, {@code false} otherwise
	 */
	private boolean existsFreeVehicleOfType(int i) {

		int startNumOfvehicles = vehicleData.get(i).getNumberOfVehicles();

		return (usedVehicles[i] < startNumOfvehicles);
	}

	/**
	 * Function which increases the count of the vehicles used but picks the
	 * best type in which the current weight and volume fits.
	 */
	private void useOneVehicle() {

		double minVolume = Double.MAX_VALUE;
		int typeOfvehicle = 0;

		for (int i = 0; i < vehicleData.size(); ++i) {
			if (vehicleCanCarry(i, currentCapacity, currentVolume)
					&& existsFreeVehicleOfType(i)) {
				if (vehicleData.get(i).getVolumeCap() < minVolume) {
					typeOfvehicle = i;
					minVolume = vehicleData.get(i).getVolumeCap();
				}
			}
		}

		usedVehicles[typeOfvehicle]++;

	}

	/**
	 * Checks if transition from the current location to the next is feasible.
	 * If all of the capacity, time window and other constraints are fulfilled.
	 * 
	 * @param current
	 *            location
	 * @param next
	 *            destined location
	 * @return {@code true} if the transition is feasible, {@code false}
	 *         otherwise
	 */
	private boolean transitionIsFeasibile(Location current, Location next) {

		return (canArriveBeforeWindowEnds(current, next)
				&& transitionDoesntOverflow(current, next) && durationLimit(getArrival(
						current, next)));

	}

	/**
	 * Maximum driving time of the driver.
	 * 
	 * @param curTime
	 *            current day-time
	 * @return {@code true} if driving time is within the constraint,
	 *         {@code false} otherwise
	 */
	private boolean durationLimit(int curTime) {

		return ((curTime - startTime) <= maxDrivingTime);
	}

	/**
	 * Gets the arrival time of the from the current location to the next taking
	 * into account the time needed to return to the warehouse. This function is
	 * used only for the feasibility check.
	 * 
	 * @param current
	 *            location
	 * @param next
	 *            destined location
	 * @return arrival time in absolute day-time
	 */
	private int getArrival(Location current, Location next) {
		String key = getKey(current, next);

		int arrivalTime = Math.max(currentTime
				+ distanceTimeMatrix.get(key).getDuration(),
				next.getWindowStart());

		key = getKey(next, warehouse);

		return arrivalTime + distanceTimeMatrix.get(key).getDuration()
				+ next.getDeliveryTime();
	}

	/**
	 * Checks if vehicle can arrive before the time window of the second
	 * location ends.
	 * 
	 * @param loc1
	 *            current location
	 * @param loc2
	 *            second location
	 * @return {@code true} if vehicle can get before, {@code false} otherwise
	 */
	private boolean canArriveBeforeWindowEnds(Location loc1, Location loc2) {
		String key = getKey(loc1, loc2);
		int arrivalTime = currentTime
				+ distanceTimeMatrix.get(key).getDuration();
		if (arrivalTime <= loc2.getWindowEnd()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the transition to the next location violates the capacity
	 * constraints(volume, weight).
	 * 
	 * @param loc1
	 *            current location
	 * @param loc2
	 *            destined location
	 * @return {@code true} if there's no violation, {@code false} otherwise
	 */
	private boolean transitionDoesntOverflow(Location loc1, Location loc2) {

		int potentiallyCariedWeight = currentCapacity + loc2.getCargoWeight();
		int potentiallyCariedVolume = currentVolume + loc2.getCargoVolume();

		for (int k = 0; k < vehicleData.size(); ++k) {
			if (vehicleCanCarry(k, potentiallyCariedWeight,
					potentiallyCariedVolume) && existsFreeVehicleOfType(k)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Calculates the total sum of all the factors in the probability
	 * calculation. Formula used is described in the documentation of the
	 * algorithm.
	 * 
	 * @param pheromoneMatrix
	 *            matrix holding the pheromone weights
	 * @return total sum of factors
	 */
	private double totalSumOfFactors(double[][] pheromoneMatrix) {

		double totalSum = 0;

		for (Entry<Integer, Location> pair : unvisitedLocations.entrySet()) {

			if (transitionIsFeasibile(lastVisited, pair.getValue())) {
				double tau_ij = pheromoneMatrix[lastVisited.getID()][pair
				                                                     .getKey()];
				double eta_ij = eta(lastVisited.getID(), pair.getKey());
				totalSum += Math.pow(tau_ij, alpha) * Math.pow(eta_ij, beta);
			}

		}

		return totalSum;
	}

	/**
	 * Calculates the transition probability of a single transition.
	 * 
	 * @param i
	 *            index of the current location
	 * @param j
	 *            index of the destined location
	 * @param pheromoneMatrix
	 *            needed for probability calculation
	 * @param total
	 *            total sum of factors to normalize the probability distribution
	 * @return transition probability (value in [0-1])
	 */
	public double transitionProbability(int i, int j,
			double[][] pheromoneMatrix, double total) {
		double tau_ij = pheromoneMatrix[i][j];
		double eta_ij = eta(i, j);

		return Math.pow(tau_ij, alpha) * Math.pow(eta_ij, beta) / total;
	}

	@Override
	public boolean canWalk() {

		if (unvisitedLocations.size() == 0) {

			return false;
		}
		// if there exists a free vehicle ant can continue walking
		for (int i = 0; i < vehicleData.size(); ++i) {
			if (existsFreeVehicleOfType(i) == true) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Feasibility check of the whole solution or of a single rute depending on
	 * the given flag.
	 * 
	 * @param moves
	 *            whole solution or route
	 * @param routeID
	 *            id of the route
	 * @param checkAllRoutes
	 *            route check
	 * @return {@code true} if solution/route is feasible, {@code false}
	 *         otherwise
	 */
	private boolean feasibilityCheck(List<Integer> moves, int routeID,
			boolean checkAllRoutes) {

		int curTime = startTime;
		double curWeight = 0;
		double curVolume = 0;

		Location last = warehouse;
		Location next = null;

		int routeCounter = -1;
		double weightCap = vehicleData.get(0).getWeightCap();
		double volumeCap = vehicleData.get(0).getVolumeCap();

		if (checkAllRoutes == true) {
			routeCounter = routeID;
		}

		for (Integer locationID : moves) {

			if (locationID == warehouse.getID()) {
				if (checkAllRoutes == false) {
					routeCounter++;
				}
			}

			if (locationID == warehouse.getID() && routeCounter == routeID) {
				String key = getKey(last, warehouse);
				if (curTime + distanceTimeMatrix.get(key).getDuration() > (this.startTime + this.maxDrivingTime)) {
					return false;
				}
				curWeight = 0;
				curVolume = 0;
				curTime = startTime;
				last = warehouse;
			} else if (locationID != warehouse.getID()
					&& routeCounter == routeID) {
				next = locationData.get(locationID);
				curWeight += next.getCargoWeight();
				curVolume += next.getCargoVolume();
				curTime += distanceTimeMatrix.get(getKey(last, next))
						.getDuration();
				if (curTime > next.getWindowEnd() || curWeight > weightCap
						|| curVolume > volumeCap) {
					return false;
				}
				if (curTime < next.getWindowStart()) {
					curTime += next.getWindowStart() - curTime;
				}
				curTime += next.getDeliveryTime();
				last = next;

			}
		}
		return true;
	}

	@Override
	public void insertNonVisited(List<Integer> moves) {
		// algorithm uses every vertex every time, there's no need for this
		// for (Entry<Integer, Location> pair : unvisitedLocations.entrySet()) {
		// insertLocation(pair, moves);
		// unvisitedLocations.remove(pair);
		// }

	}

	private void reverse(List<Integer> tour, int i, int j) {
		int lb = Math.min(i, j);
		int ub = i + j - lb;

		List<Integer> subList = tour.subList(lb, ub);
		Collections.reverse(subList);

	}

	private int timeDistance(List<Integer> tour, int i, int j) {
		return distanceTimeMatrix.get(
				getKey(locationData.get(tour.get(i)),
						locationData.get(tour.get(j)))).getDuration();
	}

	private void twoOpt(List<Integer> tour, boolean checkAllRoutes) {

		int tmpCost;
		int iter = 0;
		int iter2 = 0;
		int minCost = getSolutionCost(tour);
		// int a = minCost;
		List<String> tabu = new ArrayList<String>();

		iter = 0;
		iter2 = 0;
		tabu.clear();

		while (iter2 < tour.size() * 2) {
			int i = (int) (Math.random() * tour.size());
			int j = (int) (Math.random() * tour.size());

			if (tour.get(i) != 0
					&& tour.get(j) != 0
					&& i != j
					&& !tabu.contains(Integer.toString(i) + Integer.toString(j))) {
				int t = tour.get(i);
				tour.remove(i);
				tour.add(j, t);

				tmpCost = getSolutionCost(tour);

				if (feasibilityCheck(tour, 0, checkAllRoutes) == true
						&& tmpCost < minCost) {
					minCost = tmpCost;
					iter++;
				} else {
					tour.remove(j);
					tour.add(i, t);
					tabu.add(Integer.toString(i) + Integer.toString(j));

				}

			}
			if (tabu.size() > 30) {
				tabu.remove(0);

			}
			iter2++;
		}



		if (checkAllRoutes == false) {
			int a = getSolutionCost(tour);
			int n = tour.size();
			for (int i = 1; i < n - 2; ++i) {
				minCost = getSolutionCost(tour);
				int ind1 = 1, ind2 = 1;
				for (int j = i + 1; j < n - 1; ++j) {
					if (tour.get(i) != 0 && tour.get(j) != 0) {
						reverse(tour, i, j);
						tmpCost = getSolutionCost(tour);
						if (feasibilityCheck(tour, 0, checkAllRoutes) == true
								&& tmpCost < minCost) {
							minCost = tmpCost;
							ind1 = i;
							ind2 = j;
						}
						reverse(tour, i, j);
					}
				}
				reverse(tour, ind1, ind2);
			}

		}
	}

	@Override
	public void improveMoves(List<Integer> moves) {

		List<List<Integer>> routes = new ArrayList<>();

		List<Integer> route = new ArrayList<>();

		for (Integer locationID : moves) {
			if (locationID == warehouse.getID()) {
				if (route.size() > 0) {
					route.add(warehouse.getID());

					List<Integer> tmp = new ArrayList<>();
					for (Integer id : route) {
						tmp.add(id);
					}

					routes.add(tmp);
					route.clear();
				}
				route.add(warehouse.getID());

			} else {
				route.add(locationID);
			}
		}

		for (List<Integer> tour : routes) {
			twoOpt(tour, false);
		}

		moves.clear();

		for (List<Integer> tour : routes) {
			moves.add(warehouse.getID());
			for (Integer loc : tour) {
				if (loc != warehouse.getID()) {
					moves.add(loc);
				}
			}
		}
		moves.add(warehouse.getID());

		twoOpt(moves, true);

	}

	@Override
	public int getStartPosition() {
		return locationData.get(0).getID();
	}

	@Override
	public int getSolutionCost(List<Integer> moves) {

		int timeCost = 0;
		int oneRouteCost = 0;
		int curTime = startTime;
		for (int i = 0; i < moves.size() - 1; ++i) {
			Location loc1 = locationData.get(moves.get(i));
			Location loc2 = locationData.get(moves.get(i + 1));
			String key = getKey(loc1, loc2);
			int duration = distanceTimeMatrix.get(key).getDuration();
			int arrivalTime = Math.max(curTime + duration,
					loc2.getWindowStart());
			int delta = arrivalTime + loc2.getDeliveryTime() - curTime;
			curTime += delta;
			oneRouteCost += delta;

			if (loc2.getID() == warehouse.getID()) {
				timeCost += oneRouteCost;
				oneRouteCost = 0;
				curTime = startTime;
			}

		}

		return timeCost;
	}

	@Override
	public double ro() {
		return ro;
	}

	@Override
	public List<Integer> getInitSolution() {
		return initSolution;
	}

	@Override
	public ACSModel copy() {
		return new VRPModel(distanceTimeMatrix, locations, vehicleData,
				initSolution, ro, alpha, beta, startTime, numOfWarehouses,
				maxDrivingTime);
	}

}