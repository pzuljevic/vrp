package hr.agrokor.acs;

import hr.agrokor.acs.model.ACSModel;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Represents an ant of {@link AntColonySystem}. This class implements a
 * behavior of sequential solution building which emulates ant ground traversal
 * and its actions.
 * 
 * Since algorithm may require several ants to work it implements the
 * {@link Callable} interface which then allows parallelized work.
 * 
 * @author vjeran
 * 
 */
public class Ant implements Callable<String> {

	/** ACSModel which implements the algorithm for a concrete problem. */
	private ACSModel model;
	/** Matrix of the pheromones. */
	private double[][] pheromoneMatrix;
	/** Pheromone evaporation parameter. */
	private double ro;
	/** List of taken moves. */
	private ArrayList<Integer> moves;

	/**
	 * Constructs an {@link Ant} from given arguments.
	 * 
	 * @param model
	 *            needed to simulate ant solution path construction
	 * @param pheromoneMatrix
	 *            needed to help the ant converge to a nice solution
	 */
	public Ant(ACSModel model, double[][] pheromoneMatrix) {
		this.model = model;
		this.ro = model.ro();
		this.pheromoneMatrix = pheromoneMatrix;
		moves = new ArrayList<>();
	}

	/**
	 * Updates the trail of the {@linkplain #pheromoneMatrix} which simulates
	 * ant droping the pheromones.
	 * 
	 * @param currentPosition
	 * @param nextPosition
	 */
	private void updateTrail(int currentPosition, int nextPosition) {
		pheromoneMatrix[currentPosition][nextPosition] *= (1 - ro);
		pheromoneMatrix[currentPosition][nextPosition] += ro * model.tauZero();
	}

	/**
	 * Starts walking across the solution space graph.
	 */
	public void startWalk() {

		moves.add(model.getStartPosition());
		int lastMove = moves.get(moves.size() - 1);
		while (model.canWalk()) {
			int nextMove;

			nextMove = model.getNextMove(pheromoneMatrix);

			moves.add(nextMove);
			// this is synchronized because no two ants should access the same
			// pheromone field at the same time
			synchronized (pheromoneMatrix) {
				updateTrail(lastMove, nextMove);
			}
			lastMove = nextMove;
		}
		moves.add(model.getNextMove(pheromoneMatrix));

		model.insertNonVisited(moves); // inserts the missing vertics

		model.improveMoves(moves); // improves the path
	}

	/**
	 * Returns the moves taken by the {@link Ant}.
	 * 
	 * @return moves
	 */
	public ArrayList<Integer> getMoves() {
		return moves;
	}

	/**
	 * Starts the ant walking!
	 */
	@Override
	public String call() throws Exception {
		try {
			startWalk();
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return "DONE";
	}

}
