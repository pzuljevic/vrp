package hr.agrokor.cluster;

import hr.agrokor.data.help.Tuple;


/**
 * Used in {@link KMeansPPAlgorithm} for generic purposes. Represents a location which is clustered
 * based on its distance to the cluster center.
 * 
 * @author vjeran
 * 
 */
public interface KMeansLocation {

	/**
	 * Squared distance to another {@link KMeansLocation}. It is not necessary for the distance
	 * metric to be Euclidean.
	 * 
	 * @param location
	 *            another location to which the squared distance is calculated
	 * @return squared distance between this {@link KMeansLocation} and given one
	 */
	double sqrDistanceTo(KMeansLocation location);

	/**
	 * Distance from this to given {@link KMeansLocation}.
	 * 
	 * @param location
	 *            another location to which the distance is measured
	 * @return distance between this {@link KMeansLocation} and given one
	 */
	double distanceTo(KMeansLocation location);

	/**
	 * Returns the {@code (x, y)} coordinates of this {@link KMeansLocation}.
	 * 
	 * @return {@code (x, y)} coordinates
	 */
	Tuple<Double, Double> getCoordinates();

}
