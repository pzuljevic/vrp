package hr.agrokor.cluster;

import hr.agrokor.data.help.Tuple;
import hr.agrokor.data.model.Location;

public class ClusterLocation implements KMeansLocation {

	private Location loc;
	private double x;
	private double y;

	public ClusterLocation(Location loc) {
		this.loc = loc;
		this.x = Double.parseDouble(loc.getGPSLatitude());
		this.y = Double.parseDouble(loc.getGPSLongitude());
	}

	@Override
	public double sqrDistanceTo(KMeansLocation location) {
		Tuple<Double, Double> coo1 = getCoordinates();
		Tuple<Double, Double> coo2 = location.getCoordinates();
		double dx = (coo2.getX() - coo1.getX());
		double dy = (coo2.getY() - coo1.getY());
		return dx * dx + dy * dy;
	}

	@Override
	public double distanceTo(KMeansLocation location) {
		return Math.sqrt(sqrDistanceTo(location));
	}

	@Override
	public Tuple<Double, Double> getCoordinates() {
		return new Tuple<Double, Double>(x, y);
	}

	public Location getLocationData() {
		return loc;
	}

}
