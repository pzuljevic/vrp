package hr.agrokor.cluster;

import hr.agrokor.data.DataManipulation;
import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;
import hr.agrokor.nn.VRPNearestNeighbour1;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainClustering {

	public static void main(String... args) {
		Map<String, Distance> distMat = DataManipulation.getDistanceMatrix(Paths.get("./data/distanceMatrix.mdat"));

		List<Location> locations = DataManipulation.getLocations(Paths.get("./data/locations.mdat"));
		List<Vehicle> vehicles = DataManipulation.getVehicleTypes(Paths.get("./data/vehicles.mdat"));

		List<ClusterLocation> locs = new ArrayList<>();

		for(Location loc: locations) {
			locs.add(new ClusterLocation(loc));
		}

		for(Location loc: locations) {
			System.out.println(loc.getGPSLatitude() + " " + loc.getGPSLongitude());
		}
		System.out.println();
		System.out.println();

		locs.remove(0);
		List<Cluster<ClusterLocation>> clusters = KMeansPPAlgorithm.getClusters(locs, 4);

		for(Cluster<ClusterLocation> cluster: clusters) {
			List<ClusterLocation> list = cluster.getMembers();
			System.out.print("0 ");
			for(ClusterLocation loc: list) {
				System.out.print(loc.getLocationData().getID() + " ");
			}
			System.out.print("0");
			System.out.println();
		}
		System.out.println();
		List<List<Location>> solution = VRPNearestNeighbour1.solve(locations, distMat, vehicles, 21600);

		for(List<Location> list: solution) {
			for(Location loc: list) {
				System.out.print(loc.getID() + " ");
			}
			System.out.println();
		}
	}
}
