package hr.agrokor.cluster;



import hr.agrokor.data.help.Tuple;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a cluster which holds locations close to it and the cluster center.
 * 
 * @author vjeran
 * 
 * @param <T>
 *            any class having the required methods implemented from {@link KMeansLocation}
 *            interface.
 */
public class Cluster<T extends KMeansLocation> {

	/** Members of this cluster. */
	private final List<T> clusterMembers;

	/** Center of this cluster. */
	private final KMeansLocation center;

	/**
	 * Constructs a new cluster from a given center.
	 * 
	 * @param center
	 *            center of the cluster
	 */
	public Cluster(final KMeansLocation center) {
		this.center = center;
		this.clusterMembers = new ArrayList<T>();
	}

	/**
	 * Constructs a new cluster from given center and locations.
	 * 
	 * @param center
	 *            center of the cluster
	 * @param clusterMembers
	 *            locations belonging to the cluster
	 */
	private Cluster(final KMeansLocation center, final List<T> clusterMembers) {
		this.center = center;
		this.clusterMembers = clusterMembers;
	}

	/**
	 * Adds a location to the cluster.
	 * 
	 * @param element
	 *            location to be added
	 */
	public void addElement(final T element) {
		clusterMembers.add(element);
	}

	/**
	 * Squared distance to a given {@link KMeansLocation}. It is not necessary for the distance
	 * metric to be Euclidean.
	 * 
	 * @param name
	 *            another location to which the squared distance is calculated
	 * @return squared distance between this {@link KMeansLocation} and given one
	 */
	public double sqrDistanceTo(final T element) {
		return center.sqrDistanceTo(element);
	}

	/**
	 * Distance to a given {@link KMeansLocation}.
	 * 
	 * @param name
	 *            another location to which the distance is measured
	 * @return distance between this {@link KMeansLocation} and given one
	 */
	public double distanceTo(final T element) {
		return center.distanceTo(element);
	}

	/**
	 * Gets the members of this cluster.
	 * 
	 * @return members of this cluster
	 */
	public List<T> getMembers() {
		return clusterMembers;
	}

	/**
	 * Used only during the creation of initial clusters. Use prohibited elsewhere.
	 * 
	 * @return center of the cluster
	 */
	@SuppressWarnings("unchecked")
	T getCenter() {
		return (T) center;
	}

	/**
	 * Creates a new cluster from this cluster's members taking the mean of their coordinates.
	 * 
	 * @return a new cluster
	 */
	public Cluster<T> createClusterFromCurrentMembers() {

		double meanX = 0.0;
		double meanY = 0.0;

		for (final T member : clusterMembers) {
			final Tuple<Double, Double> coos = member.getCoordinates();
			meanX += coos.getX();
			meanY += coos.getY();
		}
		final int n = clusterMembers.size();
		meanX /= n;
		meanY /= n;
		// need this final variable for use in anon class
		final Tuple<Double, Double> p = new Tuple<Double, Double>(meanX, meanY);
		// setting the new center
		final KMeansLocation newCenter = new KMeansLocation() {

			@Override
			public double sqrDistanceTo(final KMeansLocation location) {
				final Tuple<Double, Double> coos = location.getCoordinates();
				final double dx = coos.getX() - p.getX();
				final double dy = coos.getY() - p.getY();
				return dx * dx + dy * dy;
			}

			@Override
			public Tuple<Double, Double> getCoordinates() {
				return p;
			}

			@Override
			public double distanceTo(final KMeansLocation location) {
				return Math.sqrt(sqrDistanceTo(location));
			}
		};

		return new Cluster<T>(newCenter, clusterMembers);
	}

	/**
	 * Copies this cluster without its members.
	 * 
	 * @return a new cluster which is an memberless copy
	 */
	public Cluster<T> copyWithoutMembers() {
		return new Cluster<>(this.center);
	}

}
