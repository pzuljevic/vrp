package hr.agrokor.cluster;

import hr.agrokor.data.help.Tuple;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Contains kMeans++ algorithm for finding clusters in data. It is different from ordinary kMeans
 * because of the "smart" selection of starting cluster centers which allow the algorithm to
 * converge to a solution which is in worst case O(log(k)) distant from the optimal clustering. <br>
 * <br>
 * This class implements only 2D clustering but it is easy to change it to support an arbitrary
 * number of coordinates.
 * 
 * @author vjeran
 * 
 */
public class KMeansPPAlgorithm {

	/**
	 * Gets desired number of clusters with given locations grouped.
	 * 
	 * @param locations
	 *            to be clustered
	 * @param N
	 *            number of desired clusters
	 * @return clustered locations
	 */
	public static <T extends KMeansLocation> List<Cluster<T>> getClusters(final List<T> locations, final int N) {

		final List<Cluster<T>> startingClusters = getKppStartingClusterSet(locations, N);

		List<Cluster<T>> oldClusters = startingClusters;
		List<Cluster<T>> newClusters = null;
		int k = 0; // limit of iterations
		do {
			newClusters = group(locations, oldClusters);

			if (convergence(newClusters, oldClusters)) {
				return newClusters;
			}

			oldClusters = newClusters;
			k++;
		} while (k < 100000);

		return oldClusters;
	}

	/**
	 * Determines if solution converges.
	 * 
	 * @param newClusters
	 *            newly found clusters with their new centers
	 * @param oldClusters
	 *            previous clusters
	 * @return {@code true} if cluster centers are close enough, {@code false} otherwise
	 */
	private static <T extends KMeansLocation> boolean convergence(final List<Cluster<T>> newClusters,
			final List<Cluster<T>> oldClusters) {

		final Iterator<Cluster<T>> iterator1 = oldClusters.iterator();
		final Iterator<Cluster<T>> iterator2 = newClusters.iterator();
		for (; iterator1.hasNext() && iterator2.hasNext();) {

			final Cluster<T> cluster1 = iterator1.next();

			final Cluster<T> cluster2 = iterator2.next();
			// if distance between clusters is larger than current threshold solution didn't
			// converge
			if (cluster2.getCenter().distanceTo(cluster1.getCenter()) > 1E-3) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Groups the locations into their closest clusters.
	 * 
	 * @param locations
	 *            to be clustered
	 * @param clusters
	 *            previously found clusters
	 * @return new clusters created from the locations' coordinate means
	 */
	private static <T extends KMeansLocation> List<Cluster<T>> group(final List<T> locations,
			final List<Cluster<T>> clusters) {

		// creating new clusters for persistence
		final List<Cluster<T>> newClusters = new ArrayList<>();

		for (final Cluster<T> cluster : clusters) {
			newClusters.add(cluster.copyWithoutMembers());
		}

		// adding new members
		for (final T location : locations) {
			final Cluster<T> cluster = minByDistance(location, newClusters);
			cluster.addElement(location);
		}

		List<Cluster<T>> finalClusters = new ArrayList<>();
		for (final Cluster<T> cluster : newClusters) {
			finalClusters.add(cluster.createClusterFromCurrentMembers());
		}

		return finalClusters;
	}


	/**
	 * Gets the list of initial clusters of a desired size.
	 * 
	 * @param locations
	 *            which are going to be clustered
	 * @param N
	 * @return
	 */
	private static <T extends KMeansLocation> List<Cluster<T>> getKppStartingClusterSet(final List<T> locations,
			final int N) {

		final Random rand = new Random();

		// copying the list for persistence
		final List<T> locs = new ArrayList<>(locations);

		// choosing first cluster randomly from the list of locations
		final int index = rand.nextInt(locs.size());
		final Cluster<T> first = new Cluster<>(locs.get(index));

		// cluster list
		final List<Cluster<T>> clusters = new ArrayList<>();

		// location added to cluster and removed from the location list
		clusters.add(first);
		locs.remove(index);

		while (clusters.size() != N) {
			final List<Tuple<T, Double>> wlocs = getWeightedLocations(locs, clusters);
			final long total = (long) getMaxDistributionValue(wlocs);

			final long probIndex = Math.abs(rand.nextLong()) % total;
			// determines the index of the new random cluster

			final Tuple<T, Integer> newCluster = getRandomLocation(wlocs, probIndex);
			// new cluster found added to cluster list and removed from location list
			clusters.add(new Cluster<T>(newCluster.getX()));
			locs.remove(newCluster.getY()); // this introduces O(N*n) complexity where N is the
			// number of clusters and n number of locations

		}

		return clusters;

	}

	/**
	 * Gets the random location paired with the index for easier removal.
	 * 
	 * @param wlocs
	 *            weighted locations
	 * @param probIndex
	 *            index based on the probability distribution
	 * @return location with index
	 */
	private static <T> Tuple<T, Integer> getRandomLocation(final List<Tuple<T, Double>> wlocs, final long probIndex) {
		double prob = 0.0;
		for (int i = 0; i < wlocs.size(); i++) {
			final Tuple<T, Double> loc = wlocs.get(i);
			prob += loc.getY();
			if (probIndex < prob) {
				return new Tuple<T, Integer>(loc.getX(), i);
			}
		}

		// never reached
		return new Tuple<T, Integer>(wlocs.get(0).getX(), 0);
	}

	/**
	 * Gets the locations with the distance to their closest {@link Cluster}.
	 * 
	 * @param locs
	 *            list of all locations
	 * @param clusters
	 *            list of all picked clusters
	 * @return locations with the distances to their closest cluster, tupled
	 */
	private static <T extends KMeansLocation> List<Tuple<T, Double>> getWeightedLocations(final List<T> locs,
			final List<Cluster<T>> clusters) {
		final List<Tuple<T, Double>> list = new ArrayList<>();
		for (final T loc : locs) {
			final Cluster<T> closest = minByDistance(loc, clusters);
			list.add(new Tuple<T, Double>(loc, closest.distanceTo(loc)));
		}

		return list;
	}

	/**
	 * Gets the maximum distribution value of weighted locations.
	 * 
	 * @param wlocs
	 *            tupled location with its distance to closest cluster
	 * @return maximum distribution value (integral over the distribution function)
	 */
	private static <T extends KMeansLocation> double getMaxDistributionValue(final List<Tuple<T, Double>> wlocs) {
		double total = 0.0;
		for (final Tuple<T, Double> tuple : wlocs) {
			total += tuple.getY();
		}
		return total;
	}

	/**
	 * Returns the {@link Cluster} which has the smallest distance to the given
	 * {@link KMeansLocation}.
	 * 
	 * @param loc
	 *            given location
	 * @param clusters
	 *            list of available clusters
	 * @return cluster closest to the given location
	 */
	private static <T extends KMeansLocation> Cluster<T> minByDistance(final T loc, final List<Cluster<T>> clusters) {

		// initializing cluster
		Cluster<T> closestCluster = clusters.get(0);
		double minDistance = closestCluster.sqrDistanceTo(loc);

		/*
		 * minimizing by squared distance because dist(x1, x2) < dist(x3, x4) => sqrDist(x1, x2) <
		 * sqrDist(x3, x4)
		 */
		for (final Cluster<T> cluster : clusters) {
			final double distance = cluster.sqrDistanceTo(loc);
			if (distance < minDistance) {
				minDistance = distance;
				closestCluster = cluster;
			}
		}

		return closestCluster;
	}

}
