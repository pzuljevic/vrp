package hr.agrokor.gui.model.data;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.Attribute;
import hr.agrokor.gui.model.dao.Metadata;
import hr.agrokor.gui.window.map.Drawable;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class Location extends Metadata implements Drawable {

	public static final Color HIGHLIGHT_COLOR = Color.BLACK;

	public static enum AttributeEnum {
		BUYER_ID(0), NAME(1), CITY(2), WEIGHT(3), VOLUME(4), DELIVERY_TIME(5), TIME_FRAME(
				6), LATITUDE(7), LONGITUDE(8);

		private int index;

		private AttributeEnum(int index) {
			this.index = index;
		}

		public int getIndex() {
			return this.index;
		}
	}

	private static final Attribute[] ATTRIBUTES = {
		new Attribute("ID kupca", false, String.class),
		new Attribute("Ime", false, String.class),
		new Attribute("Grad", false, String.class),
		new Attribute("Težina", true, Integer.class),
		new Attribute("Volumen", true, Integer.class),
		new Attribute("Vrijeme dostave", false, String.class),
		new Attribute("Vremenski okvir", false, String.class),
		new Attribute("Latituda", false, Integer.class),
		new Attribute("Longituda", false, Integer.class) };

	private int id;

	private String gpsLatitude;
	private String gpsLongitude;

	public Location(int id, String buyerID, String name, String city,
			int weight, int volume, String deliveryTime, String timeFrame,
			String latitude, String longitude) {
		super();

		this.id = id;
		this.gpsLatitude = latitude;
		this.gpsLongitude = longitude;

		values.put(ATTRIBUTES[Location.AttributeEnum.BUYER_ID.getIndex()],
				buyerID);
		values.put(ATTRIBUTES[Location.AttributeEnum.NAME.getIndex()], name);
		values.put(ATTRIBUTES[Location.AttributeEnum.CITY.getIndex()], city);

		values.put(ATTRIBUTES[Location.AttributeEnum.WEIGHT.getIndex()], weight);
		values.put(ATTRIBUTES[Location.AttributeEnum.VOLUME.getIndex()], volume);

		values.put(ATTRIBUTES[Location.AttributeEnum.DELIVERY_TIME.getIndex()],
				deliveryTime);
		values.put(ATTRIBUTES[Location.AttributeEnum.TIME_FRAME.getIndex()],
				timeFrame);

		values.put(ATTRIBUTES[Location.AttributeEnum.LATITUDE.getIndex()],
				Double.parseDouble(latitude));
		values.put(ATTRIBUTES[Location.AttributeEnum.LONGITUDE.getIndex()],
				Double.parseDouble(longitude));
	}

	@Override
	public Attribute[] getAttributes() {
		return ATTRIBUTES;
	}

	@Override
	public boolean setAttributeValue(Object value, int index) {
		if (super.setAttributeValue(value, index)) {
			AbstractDAO.informDataChanged();
			return true;
		} else {
			return false;
		}
	}

	public int getID() {
		return id;
	}

	public String getGPSLatitude() {
		return gpsLatitude;
	}

	public String getGPSLongitude() {
		return gpsLongitude;
	}

	public static Location parseLocation(Row row) {

		int id = (int) row.getCell(0).getNumericCellValue();
		String buyerID = row.getCell(AttributeEnum.BUYER_ID.getIndex() + 1)
				.getStringCellValue();
		String name = row.getCell(AttributeEnum.NAME.getIndex() + 1)
				.getStringCellValue();
		String city = row.getCell(AttributeEnum.CITY.getIndex() + 1)
				.getStringCellValue();

		int weight = (int) row.getCell(AttributeEnum.WEIGHT.getIndex() + 1)
				.getNumericCellValue();
		int volume = (int) row.getCell(AttributeEnum.VOLUME.getIndex() + 1)
				.getNumericCellValue();

		String deliveryTime = row.getCell(
				AttributeEnum.DELIVERY_TIME.getIndex() + 1)
				.getStringCellValue();
		String timeFrame = row.getCell(AttributeEnum.TIME_FRAME.getIndex() + 1)
				.getStringCellValue();

		Cell gpslat = row.getCell(AttributeEnum.LATITUDE.getIndex() + 1);
		Cell gpslong = row.getCell(AttributeEnum.LONGITUDE.getIndex() + 1);
		gpslat.setCellType(Cell.CELL_TYPE_STRING);
		gpslong.setCellType(Cell.CELL_TYPE_STRING);
		String latitude = gpslat.getStringCellValue();
		String longitude = gpslong.getStringCellValue();

		return new Location(id, buyerID, name, city, weight, volume,
				deliveryTime, timeFrame, latitude, longitude);
	}

	@Override
	public String toString() {
		return getAttributeName(0) + " = " + getAttributeValue(0);
	}

	@Override
	public void drawObject(Graphics2D g, JXMapKit map, boolean highlight) {
		// Get point
		final Point2D loc = map
		.getMainMap()
		.getTileFactory()
		.geoToPixel(
				new GeoPosition((double) this.getAttributeValue(7),
						(double) this.getAttributeValue(8)),
						map.getMainMap().getZoom());

		// Draw circle over location
		if (highlight) {
			g.setColor(Location.HIGHLIGHT_COLOR);
			g.fillOval((int) loc.getX() - 7, (int) loc.getY() - 7, 14, 14);
		} else {
			g.fillOval((int) loc.getX() - 5, (int) loc.getY() - 5, 10, 10);
		}
	}

}
