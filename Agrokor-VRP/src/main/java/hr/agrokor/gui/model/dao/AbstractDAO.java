package hr.agrokor.gui.model.dao;

import hr.agrokor.gui.model.data.Driver;
import hr.agrokor.gui.model.data.Location;
import hr.agrokor.gui.model.data.Route;
import hr.agrokor.gui.model.data.Vehicle;
import hr.agrokor.gui.window.map.Drawable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractDAO {

	/**
	 * Returns active DAO.
	 */
	private static AbstractDAO parent;

	/**
	 * Currently selected item.
	 */
	private Drawable selectedItem;

	/**
	 * Listeners.
	 */
	private Set<IListener> listeners;

	/**
	 * Default constructor.
	 */
	public AbstractDAO() {
		listeners = new HashSet<>();
		selectedItem = null;
		parent = this;
	}

	public Object getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(Drawable selectedItem) {
		this.selectedItem = selectedItem;
		fire();
	}

	// Abstract
	public abstract List<Route> getRoutes();

	public abstract List<Vehicle> getVehicles();

	public abstract List<Location> getLocations();

	public abstract List<Driver> getDrivers();

	/**
	 * Adds a listener.
	 * 
	 * @param listener listener
	 * @return true if listener is successfully added
	 */
	public boolean addListener(IListener listener) {
		return listeners.add(listener);
	}

	/**
	 * Removes a listener
	 * 
	 * @param listener listener
	 * @return true if listener is successfully removed
	 */
	public boolean removeListener(IListener listener) {
		return listeners.remove(listener);
	}

	/**
	 * Informs every listener.
	 */
	public void fire() {
		for (IListener listener : listeners) {
			listener.dataChanged();
		}
	}

	/**
	 * Returns currently active DAO.
	 * 
	 * @return active DAO
	 */
	public static AbstractDAO getDAO() {
		return parent;
	}

	/**
	 * Forces currently active DAO to fire.
	 */
	public static void informDataChanged() {
		if (parent != null) {
			parent.fire();
		}
	}
}
