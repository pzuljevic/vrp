package hr.agrokor.gui.model.data;

import hr.agrokor.gui.model.dao.Attribute;
import hr.agrokor.gui.model.dao.Metadata;
import hr.agrokor.gui.window.map.Drawable;

import java.awt.Graphics2D;

import org.jdesktop.swingx.JXMapKit;

public class TimedLocation extends Metadata implements Drawable {

	public static enum AttributeEnum {
		BUYER_ID(0), NAME(1), CITY(2), WEIGHT(3), VOLUME(4), DELIVERY_TIME(5), TIME_FRAME(
				6), LATITUDE(7), LONGITUDE(8), ARRIVAL_TIME(9);

		private int index;

		private AttributeEnum(int index) {
			this.index = index;
		}

		public int getIndex() {
			return this.index;
		}
	}

	private static final Attribute[] ATTRIBUTES = {new Attribute(
			"Vrijeme dolaska", false, String.class) };
	private Location location;

	public TimedLocation(Location location, String arrivalTime) {
		super();

		this.location = location;

		values.put(
				ATTRIBUTES[AttributeEnum.ARRIVAL_TIME.getIndex()
				           - location.getAttributeCount()], arrivalTime);
	}

	@Override
	public Attribute[] getAttributes() {
		Attribute[] array = new Attribute[location.getAttributeCount()
		                                  + ATTRIBUTES.length];
		int index = 0;
		for (Attribute attribute : location.getAttributes()) {
			array[index++] = attribute;
		}
		for (Attribute attribute : ATTRIBUTES) {
			array[index++] = attribute;
		}
		return array;
	}

	@Override
	public Object getAttributeValue(int index) {
		int count = location.getAttributeCount();
		if (index < count) {
			return location.getAttributeValue(index);
		} else {
			return values.get(getAttributes()[index]);
		}
	}

	@Override
	public boolean setAttributeValue(Object value, int index) {
		int count = location.getAttributeCount();
		if (index < count) {
			return location.setAttributeValue(value, index);
		} else if (value != null
				&& getAttributes()[index].getType().equals(value.getClass())) {
			values.put(getAttributes()[index], value);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void drawObject(Graphics2D g, JXMapKit map, boolean highlight) {
		location.drawObject(g, map, highlight);
	}

}
