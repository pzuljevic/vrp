package hr.agrokor.gui.model.dao;

import hr.agrokor.gui.model.data.Driver;
import hr.agrokor.gui.model.data.Location;
import hr.agrokor.gui.model.data.Route;
import hr.agrokor.gui.model.data.Vehicle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * DAO that reads data from ".xls" file.
 * 
 * @author Vjeran Crnjak
 */
public class XLSDAO extends AbstractDAO {

	private HSSFWorkbook wb;
	private final static Path solutionPath = Paths.get("./data/saves/complete.in");
	private final static Path dataPath = Paths.get("./data/data.xls");
	private static List<Location> locations = null;
	private static List<Vehicle> vehicles = null;
	private static List<Route> routes = null;
	private static List<Driver> drivers = null;

	public enum Sheets {
		Locations(0), Vehicles(1);

		int sheetNumber;

		Sheets(int sheetNumber) {
			this.sheetNumber = sheetNumber;
		}

		public int getSheetNumber() {
			return this.sheetNumber;
		}
	}

	public XLSDAO() {
		this(dataPath);
	}

	/**
	 * Constructs from given path.
	 * 
	 * @param dataPath path to .xsl file
	 */
	public XLSDAO(Path dataPath) {
		try {

			this.wb = new HSSFWorkbook(Files.newInputStream(dataPath));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Route> getRoutes() {
		if (routes != null) {
			return routes;
		} else {

			List<String> lines = null;
			try {
				lines = Files
						.readAllLines(solutionPath, StandardCharsets.UTF_8);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<Route> routes = new ArrayList<>();
			List<Location> locations = getLocations();
			for (String line : lines) {
				routes.add(Route.parseRoute(line, locations));
			}
			XLSDAO.routes = routes;
			return routes;
		}
	}

	@Override
	public List<Vehicle> getVehicles() {
		if (vehicles != null) {
			return vehicles;
		} else {
			Sheet locSheet = wb.getSheetAt(Sheets.Vehicles.getSheetNumber());
			List<Vehicle> vehicles = new ArrayList<>();
			for (Row row : locSheet) {
				vehicles.add(Vehicle.parseVehicle(row));
			}
			XLSDAO.vehicles = vehicles;
			return vehicles;
		}
	}

	@Override
	public List<Location> getLocations() {
		if (locations != null) {
			return locations;
		} else {
			Sheet locSheet = wb.getSheetAt(Sheets.Locations.getSheetNumber());
			List<Location> locations = new ArrayList<>();
			for (Row row : locSheet) {
				locations.add(Location.parseLocation(row));
			}
			XLSDAO.locations = locations;
			return locations;
		}
	}

	@Override
	public List<Driver> getDrivers() {
		if (drivers != null) {
			return drivers;
		} else {
			List<Route> routes = getRoutes();
			List<Driver> drivers = new ArrayList<>();
			for (Route route : routes) {
				drivers.add(new Driver(route));
			}
			XLSDAO.drivers = drivers;
			return drivers;
		}
	}

}
