/**
 * Contains classes that model data used by GUI.
 * @author Filip Rudan
 */
package hr.agrokor.gui.model.data;