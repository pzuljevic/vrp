package hr.agrokor.gui.model.dao;

/**
 * Implements listener for {@link AbstractDAO} model. DAO notifies implementing
 * class of any changes to data contained within DAO itself.
 * 
 * @author Filip Rudan
 * @see AbstractDAO
 */
public interface IListener {

	/**
	 * Called when data contained within DAO is modified.
	 */
	void dataChanged();
}
