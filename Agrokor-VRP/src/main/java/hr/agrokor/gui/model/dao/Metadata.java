package hr.agrokor.gui.model.dao;

import java.util.HashMap;
import java.util.Map;

/**
 * Models data with attributes.
 * @author Filip Rudan
 * 
 */
public abstract class Metadata {

	/**
	 * Maps values to attributes.
	 */
	protected Map<Attribute, Object> values;

	/**
	 * Default constructor.
	 */
	public Metadata() {
		super();

		this.values = new HashMap<>();
	}

	/**
	 * Returns all attributes of given object;
	 * 
	 * @return
	 */
	public abstract Attribute[] getAttributes();

	/**
	 * Returns value mapped to the attribute.
	 * 
	 * @param index index of attribute
	 * @return Value of attribute
	 */
	public Object getAttributeValue(int index) {
		return values.get(getAttributes()[index]);
	}

	/**
	 * Returns number of attributes.
	 * 
	 * @return attribute count
	 */
	public int getAttributeCount() {
		return getAttributes().length;
	}

	/**
	 * Returns attribute name.
	 * 
	 * @param index index of attribute
	 * @return attribute name
	 */
	public String getAttributeName(int index) {
		return getAttributes()[index].getName();
	}

	/**
	 * Returns true if attribute is editable.
	 * 
	 * @param index index of attribute
	 * @return value
	 */
	public boolean isAttributeEditable(int index) {
		return getAttributes()[index].isEditable();
	}

	/**
	 * Returns attribute class type.
	 * 
	 * @param index index of attribute
	 * @return attribute class type
	 */
	public Class<?> getAttributeType(int index) {
		return getAttributes()[index].getType();
	}

	/**
	 * Sets attribute value.
	 * 
	 * @param value new value
	 * @param index index of attribute
	 * @return true if change is accepted
	 */
	public boolean setAttributeValue(Object value, int index) {
		if (value != null
				&& getAttributes()[index].getType().equals(value.getClass())) {
			values.put(getAttributes()[index], value);
			return true;
		} else {
			return false;
		}
	}
}
