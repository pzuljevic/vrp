package hr.agrokor.gui.model.dao;

/**
 * Attribute model.
 * 
 * @author Filip Rudan
 * 
 */
public class Attribute {

	/**
	 * Attribute name.
	 */
	private String name;
	/**
	 * Attribute editability.
	 */
	private boolean editable;
	/**
	 * Attribute class type.
	 */
	private Class<?> type;

	/**
	 * Default constructor.
	 * 
	 * @param name name
	 * @param editable true if attribute is editable
	 * @param type class type
	 */
	public Attribute(String name, boolean editable, Class<?> type) {
		super();
		this.name = name;
		this.editable = editable;
		this.type = type;
	}

	/**
	 * Return attribute name.
	 * 
	 * @return attribute name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return attribute editability.
	 * 
	 * @return editability
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Return attribute class type.
	 * 
	 * @return class type
	 */
	public Class<?> getType() {
		return type;
	}

}
