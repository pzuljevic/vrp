package hr.agrokor.gui.model.data;

import hr.agrokor.data.DataManipulation;
import hr.agrokor.data.DataManipulation.Data;
import hr.agrokor.data.model.Distance;
import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.Attribute;
import hr.agrokor.gui.model.dao.Metadata;
import hr.agrokor.gui.model.dao.SerializableList;
import hr.agrokor.gui.window.map.Drawable;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.Map;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.mapviewer.GeoPosition;

public class Route extends Metadata implements Drawable {

    public static final Color HIGHLIGHT_COLOR = Color.BLACK;
    private static final float DISTINCT_COLOR_COUNT = 12;

    public static enum AttributeEnum {
        ID(0), VISIBLE(1), COLOR(2), LOCATIONS(3), LOCATIONS_COUNT(4), ROUTE_LENGTH(
                5), ROUTE_ETA(6), NIGHT_SHIFT(7), DELIVERY_WEIGHT(8), DELIVERY_VOLUME(
                9), START_TIME(10);

        private int index;

        private AttributeEnum(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

    private static final Attribute[] ATTRIBUTES = {
            new Attribute("ID", false, Long.class),
            new Attribute("Prikaži na karti", true, Boolean.class),
            new Attribute("Boja", true, Color.class),
            new Attribute("Lista lokacija", false, List.class),
            new Attribute("Broj lokacija", false, Integer.class),
            new Attribute("Duljina rute", false, Integer.class),
            new Attribute("Trajanje rutes", false, String.class),
            new Attribute("Noćna vožnja", false, Boolean.class),
            new Attribute("Težina dostave", false, Integer.class),
            new Attribute("Volumen dostave", false, Integer.class),
            new Attribute("Vrijeme početkas", false, Integer.class) };

    public Route(long id, List<Location> locations, boolean nightShift,
            int locationsCount, int routeLength, String routeETA,
            int deliveryWeight, int deliveryVolume, int startTime) {
        super();

        values.put(ATTRIBUTES[Route.AttributeEnum.ID.getIndex()], id);
        values.put(ATTRIBUTES[Route.AttributeEnum.VISIBLE.getIndex()], true);
        values.put(ATTRIBUTES[Route.AttributeEnum.COLOR.getIndex()],
                getRandomColor());
        values.put(
                ATTRIBUTES[Route.AttributeEnum.LOCATIONS.getIndex()],
                locations != null ? getTimedLocs(locations, startTime,
                        DataManipulation.getDistanceMatrix(Data.MATRIX
                                .getPath())) : new SerializableList<>());
        values.put(ATTRIBUTES[Route.AttributeEnum.LOCATIONS_COUNT.getIndex()],
                locationsCount);
        values.put(ATTRIBUTES[Route.AttributeEnum.ROUTE_LENGTH.getIndex()],
                routeLength);
        values.put(ATTRIBUTES[Route.AttributeEnum.ROUTE_ETA.getIndex()],
                routeETA);
        values.put(ATTRIBUTES[Route.AttributeEnum.NIGHT_SHIFT.getIndex()],
                nightShift);
        values.put(ATTRIBUTES[Route.AttributeEnum.DELIVERY_WEIGHT.getIndex()],
                deliveryWeight);
        values.put(ATTRIBUTES[Route.AttributeEnum.DELIVERY_VOLUME.getIndex()],
                deliveryVolume);
        values.put(ATTRIBUTES[Route.AttributeEnum.START_TIME.getIndex()],
                startTime);
    }

    private List<TimedLocation> getTimedLocs(List<Location> locations,
            int startTime, Map<String, Distance> distanceMatrix) {
        int currentTime = startTime;
        List<TimedLocation> tlocs = new SerializableList<>();
        List<hr.agrokor.data.model.Location> locs = DataManipulation
                .getLocations(Data.LOCS.getPath());
        int n = locations.size();

        if (n > 2) {
            boolean waitFlag = false;
            tlocs.add(new TimedLocation(locations.get(0),
                    parseTime(currentTime)));
            for (int i = 0; i < n - 2; i++) {
                hr.agrokor.data.model.Location prevLoc = locs.get(locations
                        .get(i).getID());

                hr.agrokor.data.model.Location curLoc = locs.get(locations.get(
                        i + 1).getID());
                int ttime = getTravelTime(prevLoc, curLoc, distanceMatrix);
                // calculating arrival time to the current location after
                // delivery
                // is over on the previous location
                int arrivalTime = currentTime + prevLoc.getDeliveryTime()
                        + ttime;
                // if this arrival time is before the windowStart waitFlag!
                if (arrivalTime < curLoc.getWindowStart()) {
                    waitFlag = true;
                    currentTime = curLoc.getWindowStart();
                } else {
                    currentTime = arrivalTime;
                }
                // we use the arrivalTime but with a star, current time is
                // updated to the window start
                // next iteration delivery time will be added for the prev and
                // travelling time to the
                // "current", after that the process is repeated.
                tlocs.add(new TimedLocation(locations.get(i + 1),
                        parseTime(arrivalTime) + (waitFlag ? "*" : "")));
                waitFlag = false;
            }

            hr.agrokor.data.model.Location prevLoc = locs.get(locations.get(
                    n - 2).getID());
            currentTime = currentTime
                    + prevLoc.getDeliveryTime()
                    + getTravelTime(locs.get(locations.get(n - 2).getID()),
                            locs.get(locations.get(0).getID()), distanceMatrix);
            tlocs.add(new TimedLocation(locations.get(0),
                    parseTime(currentTime)));
        }
        return tlocs;
    }

    private String getKey(hr.agrokor.data.model.Location loc1,
            hr.agrokor.data.model.Location loc2) {
        StringBuilder sb = new StringBuilder();
        sb.append(loc1.getGPSLatitude()).append(',')
                .append(loc1.getGPSLongitude()).append(',')
                .append(loc2.getGPSLatitude()).append(',')
                .append(loc2.getGPSLongitude());

        return sb.toString();
    }

    private int getTravelTime(hr.agrokor.data.model.Location loc1,
            hr.agrokor.data.model.Location loc2, Map<String, Distance> distMat) {
        String key = getKey(loc1, loc2);
        int travelTime = distMat.get(key).getDuration();
        return travelTime;
    }

    @Override
    public Attribute[] getAttributes() {
        return ATTRIBUTES;
    }

    @Override
    public boolean setAttributeValue(Object value, int index) {
        if (super.setAttributeValue(value, index)) {
            AbstractDAO.informDataChanged();
            return true;
        } else {
            return false;
        }
    }

    // Color generator
    private static float nextColor = 0.0f;
    private static final float colorIncrement = 1.0f / DISTINCT_COLOR_COUNT;

    private static Color getRandomColor() {
        return Color.getHSBColor(nextColor += colorIncrement, 0.9f, 0.7f);
    }

    @Override
    public void drawObject(Graphics2D g, JXMapKit map, boolean highlight) {
        if (!(boolean) this.getAttributeValue(Route.AttributeEnum.VISIBLE
                .getIndex())) {
            return;
        }

        // Set drawing style
        if (highlight) {
            g.setColor(Route.HIGHLIGHT_COLOR);
            g.setStroke(new BasicStroke(3));
        } else {
            g.setColor((Color) this.getAttributeValue(Route.AttributeEnum.COLOR
                    .getIndex()));
            g.setStroke(new BasicStroke(2));
        }

        @SuppressWarnings("unchecked")
        List<TimedLocation> locations = (List<TimedLocation>) this
                .getAttributeValue(Route.AttributeEnum.LOCATIONS.getIndex());
        for (int index = 0; index < locations.size() - 1; index++) {
            // Get starting point
            final Point2D start = map
                    .getMainMap()
                    .getTileFactory()
                    .geoToPixel(
                            new GeoPosition((double) locations.get(index)
                                    .getAttributeValue(7), (double) locations
                                    .get(index).getAttributeValue(8)),
                            map.getMainMap().getZoom());
            // Get ending point
            final Point2D end = map
                    .getMainMap()
                    .getTileFactory()
                    .geoToPixel(
                            new GeoPosition((double) locations.get(index + 1)
                                    .getAttributeValue(7), (double) locations
                                    .get(index + 1).getAttributeValue(8)),
                            map.getMainMap().getZoom());

            // Draw line between points
            g.drawLine((int) start.getX(), (int) start.getY(),
                    (int) end.getX(), (int) end.getY());

            // Draw circle over location
            ((Drawable) locations.get(index)).drawObject(g, map, false);
        }
    }

    // Parse route
    private static int idCounter = 0;

    public static Route parseRoute(String line, List<Location> locs) {
        String[] elems = line.split("\\s+");

        List<Location> routeLocs = new SerializableList<>();
        int k = 5; // length, time, weight, volume
        for (int i = 0; i < elems.length - k; i++) {
            int locIndex = Integer.parseInt(elems[i]);
            routeLocs.add(locs.get(locIndex));
        }
        int n = elems.length;
        int length = Integer.parseInt(elems[n - 5]) / 1000;
        int time = Integer.parseInt(elems[n - 4]);
        int weight = Integer.parseInt(elems[n - 3]);
        int volume = Integer.parseInt(elems[n - 2]);
        int startTime = Integer.parseInt(elems[n - 1]);
        String strTime = parseTime(time);
        return new Route(idCounter++, routeLocs, false, routeLocs.size() - 2,
                length, strTime, weight, volume, startTime);

    }

    public static String parseTime(int seconds) {
        int secs = (seconds) % 60;
        int minutes = ((seconds / 60) % 60);
        int hours = ((seconds / 3600) % 24);

        return hours + ":" + minutes + ":" + secs;
    }

    @Override
    public String toString() {
        return getAttributeName(0) + " = " + getAttributeValue(0);
    }

}
