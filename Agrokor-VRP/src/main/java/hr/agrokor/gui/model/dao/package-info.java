/**
 * Contains DAO classes used by GUI.
 * @author Filip Rudan
 */
package hr.agrokor.gui.model.dao;