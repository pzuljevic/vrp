/**
 * Contains classes defining models used GUI.
 * @author Filip Rudan
 */
package hr.agrokor.gui.model;