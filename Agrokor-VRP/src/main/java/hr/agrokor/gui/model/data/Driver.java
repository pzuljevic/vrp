package hr.agrokor.gui.model.data;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.Attribute;
import hr.agrokor.gui.model.dao.Metadata;

public class Driver extends Metadata {

	public static enum AttributeEnum {
		ID(0), ROUTE(1), START_TIME(2), END_TIME(3), DRIVING_TIME(4);

		private int index;

		private AttributeEnum(int index) {
			this.index = index;
		}

		public int getIndex() {
			return this.index;
		}
	}

	private static final Attribute[] ATTRIBUTES = {
		new Attribute("ID", false, Long.class),
		new Attribute("Ruta", false, Route.class),
		new Attribute("Početno vrijeme", false, String.class),
		new Attribute("Konačno vrijeme", false, String.class),
		new Attribute("Vrijeme vožnje", false, String.class) };

	public Driver(Route route) {
		super();

		String startTime = Route.parseTime((int) route
				.getAttributeValue(Route.AttributeEnum.START_TIME.getIndex()));
		String endTime = Route.parseTime((int) route
				.getAttributeValue(Route.AttributeEnum.START_TIME.getIndex())
				+ getSeconds((String) route
						.getAttributeValue(Route.AttributeEnum.ROUTE_ETA
								.getIndex())));
		// Should exclude sea time
		String drivingTime = (String) route
		.getAttributeValue(Route.AttributeEnum.ROUTE_ETA.getIndex());

		values.put(ATTRIBUTES[Driver.AttributeEnum.ID.getIndex()],
				route.getAttributeValue(Route.AttributeEnum.ID.getIndex()));
		values.put(ATTRIBUTES[Driver.AttributeEnum.ROUTE.getIndex()], route);
		values.put(ATTRIBUTES[Driver.AttributeEnum.START_TIME.getIndex()],
				startTime);
		values.put(ATTRIBUTES[Driver.AttributeEnum.END_TIME.getIndex()],
				endTime);
		values.put(ATTRIBUTES[Driver.AttributeEnum.DRIVING_TIME.getIndex()],
				drivingTime);

	}

	private int getSeconds(String deliveryTime) {
		String[] arr = deliveryTime.split(":");
		int hour = Integer.parseInt(arr[0]);
		int minutes = Integer.parseInt(arr[1]);
		int seconds = Integer.parseInt(arr[2]);
		return hour * 3600 + minutes * 60 + seconds;
	}

	@Override
	public Attribute[] getAttributes() {
		return ATTRIBUTES;
	}

	@Override
	public boolean setAttributeValue(Object value, int index) {
		if (super.setAttributeValue(value, index)) {
			AbstractDAO.informDataChanged();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return getAttributeName(0) + " = " + getAttributeValue(0);
	}

}
