package hr.agrokor.gui.model.dao;

import java.util.ArrayList;

/**
 * Modifies {@link ArrayList} string representation.
 * 
 * @author Filip Rudan
 * 
 * @param <E> data type
 */
public class SerializableList<E> extends ArrayList<E> {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * String representation of list.
	 */
	public static final String TEXT = "Prikaži # lokacija...";

	@Override
	public String toString() {
		return TEXT.replace("#", Integer.toString(size()));
	}
}
