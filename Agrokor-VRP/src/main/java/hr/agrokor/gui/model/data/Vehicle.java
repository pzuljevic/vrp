package hr.agrokor.gui.model.data;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.Attribute;
import hr.agrokor.gui.model.dao.Metadata;

import org.apache.poi.ss.usermodel.Row;

public class Vehicle extends Metadata {

    public static enum AttributeEnum {
        NAME(0), WEIGHT(1), VOLUME(2), VEHICLE_COUNT(3);

        private int index;

        private AttributeEnum(int index) {
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

    private static final Attribute[] ATTRIBUTES = {
            new Attribute("Ime vozila", false, String.class),
            new Attribute("Težina", true, Integer.class),
            new Attribute("Volumen", true, Integer.class),
            new Attribute("Broj vozila", true, Integer.class) };

    public Vehicle(String name, int weight, int volume, int vehicleCount) {
        super();

        values.put(ATTRIBUTES[Vehicle.AttributeEnum.NAME.getIndex()], name);
        values.put(ATTRIBUTES[Vehicle.AttributeEnum.WEIGHT.getIndex()], weight);
        values.put(ATTRIBUTES[Vehicle.AttributeEnum.VOLUME.getIndex()], volume);
        values.put(ATTRIBUTES[Vehicle.AttributeEnum.VEHICLE_COUNT.getIndex()],
                vehicleCount);
    }

    @Override
    public Attribute[] getAttributes() {
        return ATTRIBUTES;
    }

    @Override
    public boolean setAttributeValue(Object value, int index) {
        if (super.setAttributeValue(value, index)) {
            AbstractDAO.informDataChanged();
            return true;
        } else {
            return false;
        }
    }

    public static Vehicle parseVehicle(Row row) {

        String name = row.getCell(AttributeEnum.NAME.getIndex())
                .getStringCellValue();

        int weight = (int) row.getCell(AttributeEnum.WEIGHT.getIndex())
                .getNumericCellValue();
        int volume = (int) row.getCell(AttributeEnum.VOLUME.getIndex())
                .getNumericCellValue();

        int count = (int) row.getCell(AttributeEnum.VEHICLE_COUNT.getIndex())
                .getNumericCellValue();

        return new Vehicle(name, weight, volume, count);
    }

    @Override
    public String toString() {
        return getAttributeName(0) + " = " + getAttributeValue(0);
    }
}
