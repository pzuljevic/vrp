/**
 * Contains classes used to work with windows in GUI.
 * @author Filip Rudan
 */
package hr.agrokor.gui.window;