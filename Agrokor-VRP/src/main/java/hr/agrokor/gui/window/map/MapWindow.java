package hr.agrokor.gui.window.map;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.IListener;
import hr.agrokor.gui.model.data.Location;
import hr.agrokor.gui.model.data.Route;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JInternalFrame;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapKit.DefaultProviders;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.painter.Painter;

/**
 * Window showing map and routes based on data.
 * 
 * @author Filip Rudan
 */
public class MapWindow extends JInternalFrame implements IListener {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Default zoom.
	 */
	public static final int DEFAULT_ZOOM = 8;
	/**
	 * Default frame size.
	 */
	public static final Dimension DEFAULT_SIZE = new Dimension(800, 600);
	/**
	 * Default frame location.
	 */
	public static final Point DEFAULT_LOCATION = new Point(0, 0);

	/**
	 * Data access object.
	 */
	private AbstractDAO data;
	/**
	 * Map.
	 */
	private JXMapKit map;

	/**
	 * Standard constructor, expects data so it can build map.
	 * 
	 * @param data Object containing data information
	 */
	public MapWindow(AbstractDAO data) {
		super("Karta", true, true, true, true);
		setFrameIcon(null);
		setLocation(DEFAULT_LOCATION);
		setSize(DEFAULT_SIZE);

		this.data = data;
		this.data.addListener(this);

		setContentPane(generateMap());
	}

	/**
	 * Generates frame in which map resides.
	 * 
	 * @return frame containing map
	 */
	private JXMapKit generateMap() {
		final JXMapKit jXMapKit1 = new JXMapKit();
		map = jXMapKit1;
		jXMapKit1.setDefaultProvider(DefaultProviders.OpenStreetMaps);
		jXMapKit1.setCenterPosition(getMapCenter());
		jXMapKit1.setZoom(DEFAULT_ZOOM);

		final Painter<JXMapViewer> lineOverlay = new Painter<JXMapViewer>() {

			@Override
			public void paint(Graphics2D g, final JXMapViewer map, final int w,
					final int h) {
				g = (Graphics2D) g.create();
				// Convert from view port to world bitmap
				final Rectangle rect = jXMapKit1.getMainMap()
						.getViewportBounds();
				g.translate(-rect.x, -rect.y);

				// Do the drawing
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);

				// Draw route
				for (Route route : data.getRoutes()) {
					if (route != data.getSelectedItem()) {
						route.drawObject(g, jXMapKit1, false);
					}
				}
				if (data.getSelectedItem() != null
						&& data.getSelectedItem() instanceof Drawable) {
					((Drawable) data.getSelectedItem()).drawObject(g,
							jXMapKit1, true);
				}

				g.dispose();

			}

		};

		jXMapKit1.getMainMap().setOverlayPainter(lineOverlay);

		return jXMapKit1;

	}

	/**
	 * Finds map center from list of locations.
	 * 
	 * @return map center coordinates
	 */
	private GeoPosition getMapCenter() {
		double centerLatitude = 0.0, centerLongitude = 0.0;
		int count = 0;

		for (Location location : data.getLocations()) {
			centerLatitude += (double) location
					.getAttributeValue(Location.AttributeEnum.LATITUDE
							.getIndex());
			centerLongitude += (double) location
					.getAttributeValue(Location.AttributeEnum.LONGITUDE
							.getIndex());
			count++;
		}

		return new GeoPosition(centerLatitude / count, centerLongitude / count);
	}

	@Override
	public void dataChanged() {
		map.getMainMap().repaint();

	}
}
