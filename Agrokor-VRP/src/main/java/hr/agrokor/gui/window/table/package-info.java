/**
 * Contains classes used by table windows.
 * @author Filip Rudan
 */
package hr.agrokor.gui.window.table;