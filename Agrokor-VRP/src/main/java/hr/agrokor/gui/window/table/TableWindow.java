package hr.agrokor.gui.window.table;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.Metadata;
import hr.agrokor.gui.optlink.InputOutputHandler;
import hr.agrokor.gui.window.map.Drawable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.poi.ss.usermodel.Sheet;

public class TableWindow extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Dimension DEFAULT_SIZE = new Dimension(800, 600);
	public static final Point DEFAULT_LOCATION = new Point(0, 0);

	JTable table;
	JScrollPane scrollPane;
	List<? extends Metadata> data;

	public TableWindow(String name, final List<? extends Metadata> data) {
		super(name, true, true, true, true);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setFrameIcon(null);
		setSize(DEFAULT_SIZE);
		setLocation(DEFAULT_LOCATION);

		this.data = data;

		// Generate table
		table = new JTable(new TableModel(data));

		table.setDefaultRenderer(Color.class, new ColorRenderer(true));
		table.setDefaultEditor(Color.class, new ColorEditor());

		table.getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (!e.getValueIsAdjusting()) {
							Metadata selectedItem = TableWindow.this.data
									.get(TableWindow.this.table
											.getSelectedRow());
							if (selectedItem instanceof Drawable) {
								AbstractDAO.getDAO().setSelectedItem(
										(Drawable) selectedItem);
							}
						}
					}
				});

		// When cell is double-clicked open new table if selected cell contains
		// list of metadata objects
		// When table is right-clicked open popup menu
		table.addMouseListener(new MouseAdapter() {

			// Show popup menu
			@Override
			public void mousePressed(MouseEvent e) {
				showPopup(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				showPopup(e);
			}

			private void showPopup(MouseEvent e) {
				if (e.isPopupTrigger()) {
					JPopupMenu popup = new JPopupMenu();
					popup.add(new JMenuItem(new AbstractAction("Export...") {

						/**
						 * Default UID.
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void actionPerformed(ActionEvent e) {
							JFileChooser dialog = new JFileChooser();
							if (dialog.showSaveDialog(TableWindow.this) == JFileChooser.APPROVE_OPTION) {
								List<Sheet> list = new ArrayList<>();
								list.add(InputOutputHandler.getSheet(data));
								InputOutputHandler.getFile(list, dialog
										.getSelectedFile().getAbsolutePath());
							}
						}
					}));
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			// Open new table
			@Override
			public void mouseClicked(MouseEvent e) {
				// Check if double click occured
				if (e.getClickCount() == 2) {
					// Get selected object
					JTable target = (JTable) e.getSource();
					Metadata selectedItem = TableWindow.this.data.get(target
							.getSelectedRow());
					Object selectedAttribute = selectedItem
							.getAttributeValue(target.getSelectedColumn());

					// Hacky way to check if selected item is instance of
					// "List<? extends Metadata>" because Java erases Generic
					// type in runtime... Thanks Java!! :(
					if (selectedAttribute != null
							&& selectedAttribute instanceof List<?>
					&& ((List<?>) selectedAttribute).size() != 0
					&& ((List<?>) selectedAttribute).get(0) instanceof Metadata) {
						// Create new table window
						@SuppressWarnings("unchecked")
						List<? extends Metadata> list = (List<? extends Metadata>) selectedAttribute;
						String tableName = TableWindow.this.getTitle()
								+ ": "
								+ selectedItem.getAttributeName(0)
								+ " = "
								+ selectedItem.getAttributeValue(0)
								+ " - "
								+ selectedItem.getAttributeName(target
										.getSelectedColumn());

						JInternalFrame table = new TableWindow(tableName, list);
						table.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						((JDesktopPane) TableWindow.this.getParent())
						.add(table);

						// Show extension table
						table.setVisible(true);
						try {
							table.setSelected(true);
						} catch (java.beans.PropertyVetoException exc) {
							JOptionPane.showMessageDialog(TableWindow.this,
									exc.getMessage(), "Greška",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}

		});

		// Add scroll bar
		scrollPane = new JScrollPane(table);
		scrollPane.setColumnHeaderView(table.getTableHeader());
		table.setFillsViewportHeight(true);

		this.setContentPane(scrollPane);

		pack();
	}
}
