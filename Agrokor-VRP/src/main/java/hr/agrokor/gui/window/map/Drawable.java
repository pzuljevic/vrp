package hr.agrokor.gui.window.map;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.window.table.TableWindow;

import java.awt.Graphics2D;

import org.jdesktop.swingx.JXMapKit;

/**
 * Implementing class is marked as being drawable object. Used to manipulate
 * selected items in tables on a map.
 * 
 * @author Filip Rudan
 * @see AbstractDAO
 * @see MapWindow
 * @see TableWindow
 */
public interface Drawable {

	/**
	 * Draws object on a graphics container.
	 * 
	 * @param g Graphics container
	 * @param map Map of a container
	 * @param highlight True if object is highlighted
	 */
	void drawObject(Graphics2D g, JXMapKit map, boolean highlight);
}
