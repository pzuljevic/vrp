package hr.agrokor.gui.window.table;

import hr.agrokor.gui.model.dao.Metadata;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * TableModel that can read a list of metadata objects.
 * 
 * @author Filip Rudan
 * 
 */
public class TableModel extends AbstractTableModel {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data
	 */
	private List<? extends Metadata> data;

	/**
	 * Default constructor
	 * 
	 * @param data table data
	 */
	public TableModel(List<? extends Metadata> data) {
		super();

		if (data == null) {
			throw new NullPointerException();
		}
		if (data.size() == 0) {
			throw new IllegalArgumentException("List must have non-zero items!");
		}

		this.data = data;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return data.get(0).getAttributeCount();
	}

	@Override
	public String getColumnName(int column) {
		return data.get(0).getAttributeName(column);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return data.get(rowIndex).isAttributeEditable(columnIndex);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return data.get(0).getAttributeType(columnIndex);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data.get(rowIndex).getAttributeValue(columnIndex);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		data.get(rowIndex).setAttributeValue(aValue, columnIndex);
	}
}
