package hr.agrokor.gui;

import hr.agrokor.gui.model.dao.AbstractDAO;
import hr.agrokor.gui.model.dao.XLSDAO;
import hr.agrokor.gui.optlink.InputOutputHandler;
import hr.agrokor.gui.optlink.OptimizationJob;
import hr.agrokor.gui.window.WindowManager;
import hr.agrokor.gui.window.map.MapWindow;
import hr.agrokor.gui.window.table.TableWindow;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * Main program. Starts graphics user interface of "agrokor project".
 * 
 * @author Filip Rudan
 * 
 */
public class Main extends JFrame {

    /**
     * Default UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Help file location.
     */
    public static final String HELP_FILE = "data/HelpStudioSample.chm";

    // Data
    private AbstractDAO data;

    // Desktop
    private JDesktopPane workDesk;

    // Windows
    private TableWindow routes;
    private TableWindow locations;
    private TableWindow vehicles;
    private TableWindow drivers;
    private MapWindow map;

    /**
     * Default constructor.
     */
    public Main() {
        super();

        // Load data
        try {
            loadData();
        } catch (IOException exc) {
            JOptionPane.showMessageDialog(this, exc.getMessage(), "Greška",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(DISPOSE_ON_CLOSE);
        }

        // Draw desktop
        setInitialParameters();
        initGUI();

    }

    /**
     * Loads data access object.
     * 
     * @throws IOException if there is IO error
     */
    private void loadData() throws IOException {
        data = new XLSDAO();
    }

    /**
     * Sets frame initial parameters.
     */
    private void setInitialParameters() {
        setTitle("Agrokor - VRPTW solver");
        setSize(1024, 768);
        setLocation(0, 0);

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

        });
    }

    /**
     * Initializes GUI.
     */
    private void initGUI() {
        // Add desktop handler
        workDesk = new JDesktopPane();
        this.setContentPane(workDesk);

        openWindows();

        // Add menu bar
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        buildMenuTree(menuBar);
    }

    private void openWindows() {
        // Add "Routes" window
        routes = new TableWindow("Rute", data.getRoutes());

        // Add "Locations" window
        locations = new TableWindow("Lokacije", data.getLocations());

        // Add "Vehicles" window
        vehicles = new TableWindow("Vozila", data.getVehicles());

        // Add "Drivers" window
        drivers = new TableWindow("Vozači", data.getDrivers());

        spawnWindow(drivers);
        spawnWindow(locations);
        spawnWindow(routes);
        spawnWindow(vehicles);

        // Add "Map" window
        map = new MapWindow(data);
        spawnWindow(map);
    }

    /**
     * Spawns new window on desktop.
     * 
     * @param window Window to be spawned
     */
    private void spawnWindow(JInternalFrame window) {
        workDesk.add(window);
        window.setVisible(true);
        try {
            window.setSelected(true);
        } catch (java.beans.PropertyVetoException exc) {
            JOptionPane.showMessageDialog(this, exc.getMessage(), "Greška",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Builds menu tree and options.
     * 
     * @param menuBar parent
     */
    private void buildMenuTree(JMenuBar menuBar) {
        // Menu File
        JMenu file = new JMenu("Datoteka");
        menuBar.add(file);
        file.add(new AbstractAction("Otvori...") {

            /**
             * Default UID.
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser dialog = new JFileChooser();
                if (dialog.showOpenDialog(Main.this) == JFileChooser.APPROVE_OPTION) {

                    Path out = Paths.get(dialog.getSelectedFile()
                            .getAbsolutePath());

                    Thread th = new Thread(new OptimizationJob());
                    th.start();
                }
            }

        });
        file.add(new AbstractAction("Izvezi sve...") {

            /**
             * Default UID.
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser dialog = new JFileChooser();
                if (dialog.showSaveDialog(Main.this) == JFileChooser.APPROVE_OPTION) {
                    List<Sheet> list = new ArrayList<>();
                    list.add(InputOutputHandler.getSheet(data.getDrivers()));
                    list.add(InputOutputHandler.getSheet(data.getVehicles()));
                    list.add(InputOutputHandler.getSheet(data.getLocations()));
                    list.add(InputOutputHandler.getSheet(data.getRoutes()));
                    InputOutputHandler.getFile(list, dialog.getSelectedFile()
                            .getAbsolutePath());
                }
            }

        });
        file.add(new JSeparator());
        file.add(new AbstractAction("Izlaz") {

            /**
             * Default UID.
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }

        });

        // Menu View
        JMenu view = new JMenu("Pogled");
        menuBar.add(view);
        view.add(new AbstractAction("Popločaj") {

            /**
             * Default UID.
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                WindowManager.tile(Main.this.workDesk);
            }

        });
        view.add(new AbstractAction("Kaskadiraj") {

            /**
             * Default UID.
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                WindowManager.cascade(Main.this.workDesk);
            }

        });
    }

    /**
     * Runs program.
     * 
     * @param args arguments are ignored
     */
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                new Main().setVisible(true);
            }

        });
    }

}
