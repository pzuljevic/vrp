package hr.agrokor.gui.optlink;

import hr.agrokor.gui.model.dao.Metadata;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Exports files to ".xls" format.
 * 
 * @author Vjeran Crnjak
 * 
 */
public class InputOutputHandler {

	private static int sheetNum = 0;
	private static HSSFWorkbook wb = null;

	public static Sheet getSheet(List<? extends Metadata> list) {

		if (wb == null) {
			wb = new HSSFWorkbook();
		}
		Sheet sheet = wb.createSheet("sheet" + sheetNum++);

		int n = list.size();
		for (int i = 0; i < n; i++) {
			Row row = sheet.createRow(i);
			Metadata data = list.get(i);
			int k = data.getAttributeCount();
			for (int j = 0; j < k; j++) {
				Cell cell = row.createCell(j);
				cell.setCellValue(data.getAttributeValue(j).toString());
			}
		}
		return sheet;
	}

	public static void getFile(List<Sheet> list, String filePath) {
		Path file = Paths.get(filePath);

		Workbook wb = list.get(0).getWorkbook();

		try {
			OutputStream os = Files.newOutputStream(file,
					StandardOpenOption.CREATE);
			wb.write(os);
			wb = null;
			os.flush();
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
