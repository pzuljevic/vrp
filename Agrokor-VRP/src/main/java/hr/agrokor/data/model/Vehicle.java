package hr.agrokor.data.model;

/**
 * Represents the interface of the vehicle data. Classes implementing this
 * interface hold capacity constraints and number of the vehicles.
 * 
 * @author vjeran
 * 
 */
public interface Vehicle {

	/**
	 * Gets the weight capacity of this {@link Vehicle}.
	 * 
	 * @return weight capacity
	 */
	int getWeightCap();

	/**
	 * Gets the volume capacity of this {@link Vehicle}.
	 * 
	 * @return volume capacity
	 */
	int getVolumeCap();

	/**
	 * Gets the number of {@link Vehicle}s of this type.
	 * 
	 * @return number of vehicles
	 */
	int getNumberOfVehicles();
}
