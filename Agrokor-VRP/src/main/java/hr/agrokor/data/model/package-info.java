/**
 * Contains all of the needed interfaces which model the behavior of the wanted data.
 */
package hr.agrokor.data.model;