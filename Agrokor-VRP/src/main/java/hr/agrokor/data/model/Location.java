package hr.agrokor.data.model;

import hr.agrokor.cluster.KMeansLocation;

/**
 * Represents a data model which will be used by the optimization algorithms.
 * 
 * Methods below are needed to provide basic handling of values in order for the
 * algorithms to work. Any other functionality can be added in the concrete
 * implementation.
 * 
 * @author vjeran
 * 
 */
public interface Location extends KMeansLocation {

	/**
	 * Gets the ID (should be unique) of the location.
	 * 
	 * @return id of the location
	 */
	int getID();

	/**
	 * Gets the name of the location.
	 * 
	 * @return name of the location
	 */
	String getName();

	/**
	 * Gets the GPS longitude where the location is situated.
	 * 
	 * @return gps longitude
	 */
	String getGPSLongitude();

	/**
	 * Gets the GPS latitude where the location is situated.
	 * 
	 * @return gps latitude
	 */
	String getGPSLatitude();

	/**
	 * Gets the weight of the cargo which the location requires.
	 * 
	 * @return weight of the cargo
	 */
	int getCargoWeight();

	/**
	 * Gets the volume of the cargo which the location requires.
	 * 
	 * @return volume of the cargo
	 */
	int getCargoVolume();

	/**
	 * Gets the time needed to unload the cargo from the transportation
	 * {@link Vehicle}.
	 * 
	 * @return time needed to unload the cargo
	 */
	int getDeliveryTime();

	/**
	 * Gets the start time of the window in which the transportation {@link Vehicle} can arrive.
	 * 
	 * @return
	 */
	int getWindowStart();

	/**
	 * Gets the end time of the window in which the transportation {@link Vehicle} can arrive.
	 * 
	 * @return
	 */
	int getWindowEnd();
}
