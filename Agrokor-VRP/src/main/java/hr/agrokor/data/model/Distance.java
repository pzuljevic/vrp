package hr.agrokor.data.model;

/**
 * Represents the interface of the distance data. Classes implementing this
 * interface hold spatial and temporal data.
 * 
 * @author vjeran
 * 
 */
public interface Distance {

	/**
	 * Gets the distance.
	 * 
	 * @return distance value
	 */
	int getDistance();

	/**
	 * Gets the duration.
	 * 
	 * @return duration value
	 */
	int getDuration();

}
