package hr.agrokor.data.help;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SolutionPersist {
	private static final String saveLocation = "./data/saves/";
	private static final Path solution = Paths.get("./data/saves/save0.in");

	public static void saveSolution(List<List<Integer>> solution, int costTime, int costDistance) {
		Path saveLoc = getSPath(costTime, costDistance);
		List<String> lines = getLines(solution);
		try {
			Files.write(saveLoc, lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<List<Integer>> loadSolution() {
		Path loadLoc = solution /*getLPath()*/;
		List<List<Integer>> solution = null;
		try {
			solution = parseSolution(Files.readAllLines(loadLoc, StandardCharsets.UTF_8));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return solution;
	}

	private static List<List<Integer>> parseSolution(List<String> lines) {
		List<List<Integer>> routes = new ArrayList<>();
		for (String line : lines) {
			routes.add(parseRoute(line));
		}
		return routes;
	}

	private static List<Integer> parseRoute(String line) {
		String[] elems = line.split("\\s+");
		List<Integer> route = new ArrayList<>();
		for (String string : elems) {
			route.add(Integer.parseInt(string));
		}
		return route;
	}

	private static Path getLPath() {
		return lastFileModified(saveLocation);
	}

	public static Path lastFileModified(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isFile();
			}
		});
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choise = file;
				lastMod = file.lastModified();
			}
		}
		return choise.toPath();
	}

	private static Path getSPath(int costTime, int costDistance) {
		int counter = new File(saveLocation).listFiles().length;
		String path = saveLocation+"save"+counter+"t"+costTime+"d"+costDistance+".in";
		return Paths.get(path);
	}

	private static List<String> getLines(List<List<Integer>> solution) {
		List<String> lines = new ArrayList<>();
		for (List<Integer> list : solution) {
			String routeStr = getRouteStr(list);
			lines.add(routeStr);
		}
		return lines;
	}

	private static String getRouteStr(List<Integer> list) {
		StringBuilder sb = new StringBuilder();
		for (Integer integer : list) {
			sb.append(integer).append(' ');
		}
		return sb.toString();
	}

}
