/**
 * Contains data structures which are used in order to simplify some data transactions in algorithms.
 */
package hr.agrokor.data.help;