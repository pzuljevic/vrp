package hr.agrokor.data;

import hr.agrokor.cluster.KMeansLocation;
import hr.agrokor.data.help.Tuple;
import hr.agrokor.data.model.Location;

/**
 * Contains data of a location.
 * 
 * @author vjeran
 * 
 */
public class LocationData implements Location {

	// svi su public jer bi kreiranje gettera samo sve zakompliciralo
	// sve je final jer bolje da nemamo mogućnost modificiranja varijabli(manja
	// mogućnost bugova)

	/** ID and location. */
	private final int ID;
	private final String name;

	/** GPS coordinates. */
	private final String gpsLongitude;
	private final String gpsLatitude;

	/** Weight and volume of the cargo needed. */
	private final int weight;
	private final int volume;

	/** Duration of the delivery. */
	private final int deliveryTime;

	/** Span of the time window. */
	private final int windowStart;
	private final int windowEnd;

	public LocationData(final int ID, final String gpsLongitude, final String gpsLatitude, final int weight,
			final int volume, final int deliveryTime, final int windowStart, final int windowEnd, final String location) {
		this.ID = ID;
		this.gpsLongitude = gpsLongitude;
		this.gpsLatitude = gpsLatitude;
		this.weight = weight;
		this.volume = volume;
		this.deliveryTime = deliveryTime;
		this.windowStart = windowStart;
		this.windowEnd = windowEnd;
		this.name = location;
	}

	public static LocationData parseLocationData(final String line) {

		final String[] buffer = line.split("\\s+");
		/** ID and location. */
		int ID;
		String location;

		/** GPS coordinates. */
		String gpsLongitude;
		String gpsLatitude;

		/** Weight and volume of the cargo needed. */
		int weight;
		int volume;

		/** Duration of the delivery. */
		int deliveryTime;

		/** Span of the time window. */
		int windowStart;
		int windowEnd;

		ID = Integer.parseInt(buffer[0]);
		location = buffer[1];
		weight = Integer.parseInt(buffer[2]);
		volume = Integer.parseInt(buffer[3]);
		deliveryTime = parseTimeToSeconds(buffer[4]);
		windowStart = parseTimeToSeconds(buffer[5]);
		windowEnd = parseTimeToSeconds(buffer[6]);
		gpsLatitude = buffer[7];
		gpsLongitude = buffer[8];

		return new LocationData(ID, gpsLongitude, gpsLatitude, weight, volume, deliveryTime, windowStart, windowEnd,
				location);

	}

	private static int parseTimeToSeconds(String string) {
		String[] split = string.split(":");
		int hour = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);

		return 60*(hour*60 + minutes);
	}
	/**
	 * 
	 */

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ""+ID;
	}

	@Override
	public double sqrDistanceTo(KMeansLocation location) {
		double X = Double.parseDouble(gpsLongitude);
		double Y = Double.parseDouble(gpsLatitude);

		Tuple<Double, Double> coo = location.getCoordinates();
		double dx = X - coo.getX();
		double dy = Y - coo.getY();
		return dx*dx+dy*dy;
	}


	@Override
	public double distanceTo(KMeansLocation location) {
		// TODO Auto-generated method stub
		return this.sqrDistanceTo(location);
	}

	@Override
	public Tuple<Double, Double> getCoordinates() {
		double X = Double.parseDouble(gpsLongitude);
		double Y = Double.parseDouble(gpsLatitude);
		return new Tuple<Double, Double>(X, Y);
	}

	@Override
	public int getID() {
		return ID;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getGPSLongitude() {
		return gpsLongitude;
	}

	@Override
	public String getGPSLatitude() {
		return gpsLatitude;
	}

	@Override
	public int getCargoWeight() {
		return weight;
	}

	@Override
	public int getCargoVolume() {
		return volume;
	}

	@Override
	public int getDeliveryTime() {
		return deliveryTime;
	}

	@Override
	public int getWindowStart() {
		return windowStart;
	}

	@Override
	public int getWindowEnd() {
		return windowEnd;
	}



}
