/**
 * Contains all of the methods and data structures needed to represent locations, vehicles and distance
 * for algorithm usage.
 */
package hr.agrokor.data;