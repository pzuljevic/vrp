package hr.agrokor.data;

import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides data fetching methods.
 * 
 * @author vjeran
 * 
 */
public class DataManipulation {
	/**
	 * Holds all of the required paths for the data.
	 * 
	 * @author vjeran
	 * 
	 */
	public enum Data {
		/** Matrix file location. */
		MATRIX(Paths.get("./data/distanceMatrix.mdat")),
		/** Locations file location. */
		LOCS(Paths.get("./data/locations.mdat")),
		/** Vehicles file location. */
		VEHICLES(Paths.get("./data/vehicles.mdat"));

		private Path path;

		private Data(Path path) {
			this.path = path;
		}

		public Path getPath() {
			return path;
		}

	}

	private static Map<String, Distance> distanceMatrix = null;

	/**
	 * Returns the distance matrix. Where {@link String} is a GPS coordinate and
	 * long the distance. Example of pair:
	 * "43.041874,16.199615,43.044431,16.089516" -> DistanceData(13693,1073)
	 * 
	 * @param fileName
	 *            name of the file where distance matrix is located
	 * @return mapping from gps coordinate pair to {@link Distance} information
	 */
	public static Map<String, Distance> getDistanceMatrix(final Path filePath) {
		if (distanceMatrix == null) {
			List<String> lines = null;
			try {
				lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);

			} catch (final IOException e) {
				System.err
				.println("Reading of the distance matrix file failed! ABORTING...");
				e.printStackTrace();
				System.exit(-1);
			}

			final Map<String, Distance> mappedDistanceMatrix = new HashMap<String, Distance>();
			for (final String line : lines) {

				final String[] buffer = line.split("\\s+");
				// splitedLine =
				// {"(43.041874,16.199615,43.044431,16.089516)","<->","(13693,1073)"}
				final String parsedKey = parseKey(buffer[0]);
				final Distance dist = DistanceData.parseDistanceData(buffer[2]);
				mappedDistanceMatrix.put(parsedKey, dist);
			}
			distanceMatrix = mappedDistanceMatrix;
		}
		return distanceMatrix;
	}

	/**
	 * Parses the key for the distance matrix map.
	 * 
	 * @param line
	 *            string containing the key
	 * @return key
	 */
	private static String parseKey(final String line) {
		String parsedKey = line;
		parsedKey = parsedKey.replace("(", "");
		parsedKey = parsedKey.replace(")", "");
		// parsedKey = "43.041874,16.199615,43.044431,16.089516"
		return parsedKey;
	}

	/**
	 * Gets the list of {@link Location} objects.
	 * 
	 * @param filePath
	 *            path to the locations file
	 * @return list of location data
	 */
	public static List<Location> getLocations(final Path filePath) {

		List<String> stringList = null;
		try {
			stringList = Files.readAllLines(filePath, StandardCharsets.UTF_8);

		} catch (final IOException e) {
			System.err
			.println("Reading of the locations file failed! ABORTING...");
			e.printStackTrace();
			System.exit(-1);
		}

		// buffer = {"IDNUM", "�I�I�", "NADA/TOTINA", "SPLIT", "34", "14",
		// "0:09", "07:00"
		// "19:00", "43.525112", "16.464305"}

		final List<Location> locations = new ArrayList<Location>();
		for (final String line : stringList) {
			locations.add(LocationData.parseLocationData(line));
		}

		return locations;
	}

	/**
	 * Gets the list of {@link Vehicle} objects.
	 * 
	 * @param filePath
	 *            path to the locations file
	 * @return list of location data
	 */
	public static List<Vehicle> getVehicleTypes(final Path filePath) {

		List<String> stringList = null;
		try {
			stringList = Files.readAllLines(filePath, StandardCharsets.UTF_8);

		} catch (final IOException e) {
			System.err
			.println("Reading of the vehicle file failed! ABORTING...");
			e.printStackTrace();
			System.exit(-1);
		}

		final List<Vehicle> vehicles = new ArrayList<Vehicle>();
		for (final String line : stringList) {
			vehicles.add(VehicleData.parseVehicleData(line));
		}

		return vehicles;
	}

}
