package hr.agrokor.data;

import hr.agrokor.data.model.Vehicle;

public class VehicleData implements Vehicle {

	/** Volume capacity of the vehicle. */
	private final int volumeCap;

	/** Weight capacity of the vehicle. */
	private final int weightCap;

	/** Number of given vehicles. */
	private final int numberOfVehicles;

	public VehicleData(final int weightCap, final int volumeCap, final int numberOfVehicles) {
		this.weightCap = weightCap;
		this.volumeCap = volumeCap;
		this.numberOfVehicles = numberOfVehicles;
	}

	public static VehicleData parseVehicleData(final String line) {

		final String[] buffer = line.split("\\s+");
		int capacityWeight, capacityVolume, numOfGivenVehicles;

		capacityWeight = Integer.parseInt(buffer[1]);
		capacityVolume = Integer.parseInt(buffer[2]);
		numOfGivenVehicles = Integer.parseInt(buffer[3]);

		return new VehicleData(capacityWeight, capacityVolume, numOfGivenVehicles);

	}

	@Override
	public int getVolumeCap() {
		return volumeCap;
	}

	@Override
	public int getWeightCap() {
		return weightCap;
	}

	@Override
	public int getNumberOfVehicles() {
		return numberOfVehicles;
	}

}
