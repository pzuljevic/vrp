package hr.agrokor.data;

import hr.agrokor.data.model.Distance;

/**
 * Contains information about the distance and duration of two locations.
 * 
 * @author vjeran
 * 
 */
public class DistanceData implements Distance {
	// možemo pogledati matricu i vidjeti je li udaljenosti
	// prekoračuju int limit, ako ne, onda optimizirati za
	// memoriju u int

	/** Distance in meters between two locations. */
	private final int distance;
	/** Duration time required to get from A to B. */
	private final int duration;

	public DistanceData(final int distance, final int duration) {
		this.distance = distance;
		this.duration = duration;
	}

	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public int getDuration() {
		return duration;
	}

	public static DistanceData parseDistanceData(final String line) {
		// line = "(43.041874,16.199615,43.044431,16.089516)"
		final int[] distanceDuration = parseDistDur(line);
		return new DistanceData(distanceDuration[0], distanceDuration[1]);
	}

	private static int[] parseDistDur(final String line) {

		String tmpDistDur = line;
		tmpDistDur = tmpDistDur.replace("(", "");
		tmpDistDur = tmpDistDur.replace(")", "");
		// tmp = 13693,1073

		final String[] distDur = tmpDistDur.split(",");

		final int distance = Integer.parseInt(distDur[0]);
		final int duration = Integer.parseInt(distDur[1]);
		final int[] tmpArray = { distance, duration };

		return tmpArray;
	}

}