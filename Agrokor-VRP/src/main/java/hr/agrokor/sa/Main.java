package hr.agrokor.sa;

import hr.agrokor.data.DataManipulation;
import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;
import hr.agrokor.nn.VRPNearestNeighbour1;
import hr.agrokor.sa.model.VRPModel;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class Main {

	private static Map<String, Distance> distMat = DataManipulation.getDistanceMatrix(Paths
			.get("./data/distanceMatrix.mdat"));
	private static List<Location> locations = DataManipulation.getLocations(Paths.get("./data/locations.mdat"));
	private static List<Vehicle> vehicles = DataManipulation.getVehicleTypes(Paths.get("./data/vehicles.mdat"));

	public static void main(String... args) {
		int startTime = 6*3600;
		List<List<Location>> greedySolution = VRPNearestNeighbour1.solve(locations, distMat, vehicles, startTime);
		showSolution(greedySolution);
		List<List<Location>> startSolution = extract(greedySolution);
		VRPModel model = new VRPModel(distMat, locations, vehicles, startSolution, startTime);
		SimulatedAnnealing.solve(model);


		showSolution(model);
	}





	private static void showSolution(List<List<Location>> sol) {
		for (List<Location> list : sol) {
			for (Location loc : list) {
				System.out.print(loc + " ");
			}
			System.out.println();
		}
	}





	private static void showSolution(VRPModel model) {
		System.out.println("Metraža: " + model.getTotalDistance());
		System.out.println("Sekundaža: " + model.getTimeCost());

		List<List<Location>> sol = model.getSolution();
		for (List<Location> list : sol) {
			System.out.print("0 ");
			for (Location loc : list) {
				System.out.print(loc + " ");
			}
			System.out.print("0");
			System.out.println();
		}
	}





	private static List<List<Location>> extract(List<List<Location>> solve) {
		for (List<Location> list : solve) {
			list.remove(0);
			list.remove(list.size() - 1);
		}
		return solve;
	}
}
