package hr.agrokor.sa;

import hr.agrokor.sa.model.SAModel;
import hr.agrokor.sa.model.SANeighbour;

public class SimulatedAnnealing {

	/**
	 * Returns the acceptance probability given the old, new cost and current temperature.
	 * 
	 * @param oldCost
	 *            old cost of the model
	 * @param newCost
	 *            cost of the neighbouring model
	 * @param temperature
	 *            current temperature of the annealing process
	 * @return probability of accepting the move to neighbour
	 */
	private static double probabilityAcceptance(final double oldCost, final double newCost, final double temperature) {

		if (newCost < oldCost) {
			return 1.0;
		}

		return Math.exp((oldCost - newCost) / temperature);
	}

	/**
	 * Solves the given {@link SAModel} returning the best solution found.
	 * 
	 * @param model
	 *            given model to solve
	 * @return best found solution
	 */
	public static SAModel solve(final SAModel model) {

		double T = getTemperature(model);

		SAModel bestModel = model;

		while (Math.abs(T) > 1E-15) {

			final double currCost = model.getCost();

			final SANeighbour currNeighbour = model.neighbour();

			final double newCost = currNeighbour.getCost();

			if (newCost < currCost) {
				bestModel = model.copy();
				bestModel.changeModelTo(currNeighbour);
			}

			if (probabilityAcceptance(currCost, newCost, T) > Math.random()) {
				model.changeModelTo(currNeighbour);
			}

			T = coolTemperature(T);
		}

		return bestModel;

	}

	/**
	 * Returns a potentially lower temperature.
	 * 
	 * @param temperature
	 *            current temperature
	 * @return new cooler temperature
	 */
	private static double coolTemperature(final double temperature) {
		return 0.999995 * temperature;
	}

	/**
	 * Returns the starting temperature of the given {@link SAModel}.
	 * 
	 * @param model
	 *            simulated annealing model
	 * @return starting temperature
	 */
	private static double getTemperature(final SAModel model) {

		final double P = 0.3;

		double sum = 0;

		for (int i = 0; i < 1000; ++i) {
			sum = model.neighbour().getCost() - model.getCost();
		}

		sum /= 1000.0;

		return -Math.abs(sum) / Math.log(P);
	}

}
