package hr.agrokor.sa.model;

public interface SANeighbour {

	/**
	 * Returns a cost of a neighbour state.
	 * 
	 * @return cost of a neighbour state
	 */
	public double getCost();

}
