package hr.agrokor.sa.model;

import hr.agrokor.data.help.Triple;

public class VRPNeighbor implements SANeighbour {

	public Triple<Integer, Integer, Integer> cost;
	public final int route1;
	public final int route2;
	public final int take;
	public final int insert;





	public VRPNeighbor(Triple<Integer, Integer, Integer> cost, int route1, int take, int route2, int insert) {
		this.cost = cost;
		this.route1 = route1;
		this.route2 = route2;
		this.take = take;
		this.insert = insert;
	}





	@Override
	public double getCost() {
		return VRPModel.getParamCost(cost.x, cost.y, cost.z);
	}

}
