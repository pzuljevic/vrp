package hr.agrokor.sa.model;

public interface SAModel {

	/**
	 * Returns a {@link SANeighbour} of this {@link SAModel}.
	 * 
	 * @return {@link SANeighbour} of this {@link SAModel}
	 */
	public SANeighbour neighbour();

	/**
	 * Modifies this model to the given {@link SANeighbour} of this model.
	 * 
	 * @param newNeighbour
	 *            neighbour of the {@link SAModel}
	 */
	public void changeModelTo(SANeighbour newNeighbour);

	/**
	 * Returns a cost of this {@link SAModel}
	 * 
	 * @return cost of this {@link SAModel}
	 */
	public double getCost();

	/**
	 * Returns a full copy of the {@link SAModel}
	 * 
	 * @return a full copy of the {@link SAModel}
	 */
	public SAModel copy();

}
