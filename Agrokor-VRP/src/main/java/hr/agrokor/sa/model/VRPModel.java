package hr.agrokor.sa.model;

import hr.agrokor.data.help.Triple;
import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class VRPModel implements SAModel {

	private Map<String, Distance> distanceTimeMatrix;
	private List<Location> locationData;
	private List<Vehicle> vehicleData;
	/** Routes do not contain warehouse. */
	private List<List<Location>> solution;
	private Location warehouse;
	// private int currentTime;
	private int startTime;
	private int timeCost;
	private int totalDistance;

	public List<List<Location>> getSolution() {
		return solution;
	}


	public int getTotalDistance() {
		return totalDistance;
	}


	public int getTimeCost() {
		return timeCost;
	}

	private int maxWeight;
	private int maxVolume;

	public VRPModel(Map<String, Distance> distMat, List<Location> locationData,
			List<Vehicle> vehicleData, List<List<Location>> solution, int startTime) {
		super();
		this.distanceTimeMatrix = distMat;
		this.locationData = locationData;
		this.vehicleData = vehicleData;
		this.solution = solution;
		this.warehouse = locationData.get(0);
		this.startTime = startTime;
		this.maxWeight = getMaxWeight(vehicleData);
		this.maxVolume = getMaxVolume(vehicleData);
		this.timeCost = getSolutionTime(warehouse, solution, startTime);
		this.totalDistance = getSolutionDistance(warehouse, solution);
	}

	private int getMaxVolume(List<Vehicle> vehicleData2) {
		int max = 0;
		for (Vehicle vehicle : vehicleData2) {
			if (vehicle.getVolumeCap() > max) {
				max = vehicle.getVolumeCap();
			}
		}
		return max;
	}

	private int getMaxWeight(List<Vehicle> vehicleData2) {
		int max = 0;
		for (Vehicle vehicle : vehicleData2) {
			if (vehicle.getWeightCap() > max) {
				max = vehicle.getWeightCap();
			}
		}
		return max;
	}

	@Override
	public SANeighbour neighbour() {
		Random rand = new Random();
		Triple<Integer, Integer, Integer> nCost;
		int route1;
		int route2;
		int take;
		int insert;
		do {
			int n = solution.size();
			route1 = rand.nextInt(n);
			route2 = rand.nextInt(n);
			List<Location> routeX = solution.get(route1);
			List<Location> routeY = solution.get(route2);
			take = rand.nextInt(routeX.size());
			insert = rand.nextInt(routeY.size());

			nCost = getNewCost(routeX, take, routeY, insert);

		} while (nCost == null);


		return new VRPNeighbor(nCost, route1, take, route2, insert);
	}

	private Triple<Integer, Integer, Integer> getNewCost(List<Location> routeX, int take, List<Location> routeY, int insert) {
		Location loc = routeX.get(take);
		if (routeX == routeY && take == insert) {
			return null;
		}

		int oldTimeX = getRouteTime(warehouse, routeX, startTime);
		int oldDistanceX = getRouteDistance(warehouse, routeX);
		routeX.remove(take);
		int newTimeX = getRouteTime(warehouse, routeX, startTime);
		int newDistanceX = getRouteDistance(warehouse, routeX);
		routeX.add(take, loc);

		int oldTimeY = getRouteTime(warehouse, routeY, startTime);
		int oldDistanceY = getRouteDistance(warehouse, routeY);
		routeY.add(insert, loc);
		if (!isFeasible(warehouse, routeY, startTime)) {
			routeY.remove(insert);
			return null;
		}
		int newTimeY = getRouteTime(warehouse, routeY, startTime);
		int newDistanceY = getRouteDistance(warehouse, routeY);
		routeY.remove(insert);

		int numOfVehicles = solution.size();
		if (routeX.isEmpty()) {
			numOfVehicles -= 1;
		}
		int time = this.timeCost - oldTimeX - oldTimeY + newTimeX + newTimeY;
		int distance = this.totalDistance - oldDistanceX - oldDistanceY + newDistanceX + newDistanceY;

		return new Triple<Integer, Integer, Integer>(numOfVehicles, time, distance);
	}

	private boolean isFeasible(Location warehouse, List<Location> routeY, int start) {

		return timeWindowCheck(warehouse, routeY, start) && capacityCheck(routeY);
	}

	private boolean capacityCheck(List<Location> routeY) {
		int weight = 0;
		int volume = 0;
		for (Location loc : routeY) {
			weight += loc.getCargoWeight();
			volume += loc.getCargoVolume();
		}
		return (weight <= maxWeight && volume <= maxVolume);
	}

	public boolean timeWindowCheck(Location warehouse, List<Location> routeY, int start) {
		int currTime = start;

		for (int i = 0; i < routeY.size() - 1; i++) {
			Location loc1 = routeY.get(i);
			Location loc2 = routeY.get(i + 1);

			if (currTime + getTimeDuration(loc1, loc2) > loc2.getWindowEnd()) {
				return false;
			}
			currTime += getDeltaTime(loc1, loc2, currTime);
		}

		return true;
	}

	@Override
	public void changeModelTo(SANeighbour newNeighbour) {
		VRPNeighbor neighbor = (VRPNeighbor) newNeighbour;
		List<Location> route1 = solution.get(neighbor.route1);
		List<Location> route2 = solution.get(neighbor.route2);
		Location loc = route1.get(neighbor.take);
		route1.remove(neighbor.take);
		if (route1.isEmpty()) {
			solution.remove(neighbor.route1);
		}
		route2.add(neighbor.insert, loc);
		Triple<Integer, Integer, Integer> cost = neighbor.cost;
		this.timeCost = cost.y;
		this.totalDistance = cost.z;

	}

	@Override
	public double getCost() {
		return getParamCost(solution.size(), timeCost, totalDistance);
	}

	public static int getParamCost(int numOfVehicles, int timeCost, int distance) {
		int alpha1 = 900000;
		int alpha2 = 3;
		int alpha3 = 1;
		return alpha1 * numOfVehicles + alpha2 * timeCost + alpha3 * distance;
	}

	public int getSolutionDistance(Location warehouse, List<List<Location>> solution) {
		int totalDistance = 0;
		for (List<Location> route : solution) {
			totalDistance += getRouteDistance(warehouse, route);
		}
		return totalDistance;
	}

	private int getRouteDistance(Location warehouse2, List<Location> route) {
		if (route.isEmpty()) {
			return 0;
		}
		int routeDistance = 0;
		int delta = getDeltaDistance(warehouse, route.get(0));
		routeDistance += delta;
		for (int i = 0; i < route.size() - 1; i++) {
			delta = getDeltaDistance(route.get(i), route.get(i + 1));
			routeDistance += delta;
		}
		delta = getDeltaDistance(route.get(route.size() - 1), warehouse);
		routeDistance += delta;
		return routeDistance;
	}

	public int getSolutionTime(Location warehouse, List<List<Location>> solution, int start) {
		int totalTime = 0;
		for (List<Location> route : solution) {
			totalTime += getRouteTime(warehouse, route, start);
		}
		return totalTime;
	}

	public int getRouteTime(Location warehouse, List<Location> route, int start) {
		if (route.isEmpty()) {
			return 0;
		}

		int routeTime = start;
		int delta = getDeltaTime(warehouse, route.get(0), routeTime);
		routeTime += delta;
		for (int i = 0; i < route.size() - 1; i++) {
			delta = getDeltaTime(route.get(i), route.get(i + 1), routeTime);
			routeTime += delta;
		}
		delta = getDeltaTime(route.get(route.size() - 1), warehouse, routeTime);
		routeTime += delta;
		return routeTime - start;
	}

	private int getDeltaDistance(Location loc1, Location loc2) {
		String key = getKey(loc1, loc2);
		return distanceTimeMatrix.get(key).getDistance();
	}

	private int getDeltaTime(Location loc1, Location loc2, int currTime) {
		int duration = getTimeDuration(loc1, loc2);
		int arrivalTime = Math.max(currTime + duration, loc2.getWindowStart());
		int delta = arrivalTime - currTime + loc2.getDeliveryTime();
		return delta;
	}

	private int getTimeDuration(Location loc1, Location loc2) {
		String key = getKey(loc1, loc2);
		return distanceTimeMatrix.get(key).getDuration();
	}

	private static String getKey(Location loc1, Location loc2) {
		StringBuilder sb = new StringBuilder();

		sb.append(loc1.getGPSLatitude()).append(',').append(loc1.getGPSLongitude()).append(',').append(loc2.getGPSLatitude())
		.append(',').append(loc2.getGPSLongitude());

		return sb.toString();
	}

	@Override
	public SAModel copy() {
		return new VRPModel(distanceTimeMatrix, locationData, vehicleData, copyList(solution), startTime);
	}

	private List<List<Location>> copyList(List<List<Location>> list) {
		List<List<Location>> nList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			nList.add(new ArrayList<Location>(list.get(i)));
		}

		return nList;
	}

}
