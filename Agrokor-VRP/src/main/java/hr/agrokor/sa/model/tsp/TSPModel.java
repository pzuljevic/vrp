package hr.agrokor.sa.model.tsp;

import hr.agrokor.sa.model.SAModel;
import hr.agrokor.sa.model.SANeighbour;

public class TSPModel implements SAModel {

	private final int[] locationIds;
	private final double[][] distMatrix;

	public TSPModel(final int[] locationIds, final double[][] distMatrix) {
		this.locationIds = locationIds;
		this.distMatrix = distMatrix;
	}

	private double locationDistance(final int l1, final int l2) {
		return distMatrix[l1][l2];
	}

	@Override
	public SANeighbour neighbour() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeModelTo(final SANeighbour newNeighbour) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SAModel copy() {
		// TODO Auto-generated method stub
		return null;
	}

}
