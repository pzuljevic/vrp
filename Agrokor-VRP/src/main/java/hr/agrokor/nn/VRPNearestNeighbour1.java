package hr.agrokor.nn;

import hr.agrokor.data.model.Distance;
import hr.agrokor.data.model.Location;
import hr.agrokor.data.model.Vehicle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VRPNearestNeighbour1 {

	public static List<List<Location>> solve(List<Location> locations,
			Map<String, Distance> distMat, List<Vehicle> vehicles, int startTime) {

		int weightMax = vehicles.get(1).getWeightCap();
		int volumeMax = vehicles.get(1).getVolumeCap();

		Set<Location> leftover = new HashSet<>(locations);

		Location warehouse = locations.get(0);
		leftover.remove(warehouse);

		List<List<Location>> solution = new ArrayList<>();

		while (leftover.size() != 0) {
			List<Location> route = getRoute(warehouse, leftover, distMat, startTime, weightMax,
					volumeMax);
			solution.add(route);
		}

		return solution;
	}

	private static List<Location> getRoute(Location warehouse, Set<Location> locations,
			Map<String, Distance> distanceTimeMatrix, int startTime, int weightMax, int volumeMax) {


		Location nextLocation = warehouse;

		int currentWeight = 0, currentVolume = 0;
		int currentTime = startTime;

		List<Location> route = new ArrayList<>();
		while (nextLocation != null) {
			route.add(nextLocation);
			Location currentLocation = nextLocation;
			locations.remove(nextLocation);
			nextLocation = getBestNext(currentLocation, locations, distanceTimeMatrix, currentVolume, currentWeight,
					weightMax, volumeMax, currentTime);

			if (nextLocation != null) {
				currentTime += deltaTime(currentLocation, nextLocation, distanceTimeMatrix, currentTime);
				currentVolume += nextLocation.getCargoVolume();
				currentWeight += nextLocation.getCargoWeight();
			}
		}

		route.add(warehouse);

		return route;
	}

	private static String getKey(Location loc1, Location loc2) {
		StringBuilder sb = new StringBuilder(25);

		sb.append(loc1.getGPSLatitude()).append(',')
		.append(loc1.getGPSLongitude()).append(',')
		.append(loc2.getGPSLatitude()).append(',')
		.append(loc2.getGPSLongitude());

		return sb.toString();
	}

	private static int deltaTime(Location currLoc, Location nextLoc, Map<String, Distance> distanceTimeMatrix,
			int currTime) {

		String key = getKey(currLoc, nextLoc);
		int tripDuration = distanceTimeMatrix.get(key).getDuration();

		int deltaTime = Math.max(currTime + tripDuration, nextLoc.getWindowStart());
		deltaTime += nextLoc.getDeliveryTime() - currTime;

		return deltaTime;
	}

	private static boolean isWindowSatisfied(Location currLoc, Location nextLoc,
			Map<String, Distance> distanceTimeMatrix, int currTime) {
		String key = getKey(currLoc, nextLoc);
		int tripDuration = distanceTimeMatrix.get(key).getDuration();

		int arrivalTime = currTime + tripDuration;

		return (arrivalTime <= nextLoc.getWindowEnd());
	}

	private static boolean isCapacitySatisfied(Location nextLoc, int currWei, int currVol, int weightMax,
			int volumeMax) {

		return (nextLoc.getCargoVolume() + currVol <= volumeMax && currWei + nextLoc.getCargoWeight() <= weightMax);
	}

	private static Location getBestNext(Location currLoc, Set<Location> locations,
			Map<String, Distance> distanceTimeMatrix, int currWei, int currVol, int weightMax,
			int volumeMax, int currTime) {

		Location bestLoc = null;
		int currMin = Integer.MAX_VALUE;

		for (Location location : locations) {
			if (isFeasibile(currLoc, location, distanceTimeMatrix, currWei, currVol, weightMax, volumeMax, currTime)
					&& isCloser(currLoc, location, distanceTimeMatrix, currMin, currTime)) {
				bestLoc = location;
				currMin = deltaTime(currLoc, location, distanceTimeMatrix, currTime);
			}
		}

		return bestLoc;
	}

	private static boolean isCloser(Location currLoc, Location nextLoc, Map<String, Distance> distanceTimeMatrix,
			int currMin, int currTime) {
		int delta = deltaTime(currLoc, nextLoc, distanceTimeMatrix, currTime);
		boolean res = (delta < currMin);
		return res;
	}

	private static boolean isFeasibile(Location currLoc, Location nextLoc,
			Map<String, Distance> distanceTimeMatrix, int currWei, int currVol, int weightMax,
			int volumeMax, int currTime) {

		return (isCapacitySatisfied(nextLoc, currWei, currVol, weightMax, volumeMax) && isWindowSatisfied(currLoc,
				nextLoc, distanceTimeMatrix, currTime));
	}

}
