package hr.agrokor.nn;

import hr.agrokor.data.DistanceData;
import hr.agrokor.data.LocationData;
import hr.agrokor.data.VehicleData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VRPNearestNeighbour {

	private static boolean locationAlreadyInSolution(int locationIndeks, ArrayList<Integer> solution,
			ArrayList<LocationData> locationData) {
		if (solution.contains(locationIndeks) || locationData.get(locationIndeks).getName().equals("SKLADISTE")) {
			return true;
		}
		return false;
	}

	private static String getKey(int i, int j, ArrayList<LocationData> locationData) {
		String key = locationData.get(i).getGPSLatitude() + "," + locationData.get(i).getGPSLongitude();
		key += "," + locationData.get(j).getGPSLatitude() + "," + locationData.get(j).getGPSLongitude();
		return key;
	}

	private static int findClosestLocationTo(int currLocationIndeks, double currentTime,
			ArrayList<LocationData> locationData, Map<String, DistanceData> distanceTimeMatrix,
			List<VehicleData> vehicleData, ArrayList<Integer> solution, double currentWeight, double currentVolume,
			ArrayList<Integer> usedVehicles) {

		double minDist = Double.MAX_VALUE;

		int choosenID = 0;

		double distBetween = 0;

		String key = "";

		for (int i = 0; i < locationData.size(); ++i) {

			if (locationAlreadyInSolution(i, solution, locationData) == false
					&& transitionDoesntOverflow(i, currentWeight, currentVolume, locationData, vehicleData,
							usedVehicles)) {

				key = getKey(currLocationIndeks, i, locationData);

				double delay = 0;

				if (currentTime + distanceTimeMatrix.get(key).getDuration() < locationData.get(i).getWindowStart()) {
					delay = locationData.get(i).getWindowStart() - currentTime - distanceTimeMatrix.get(key).getDuration();
				}
				if (delay == 0) {
					delay -= locationData.get(i).getDeliveryTime() - distanceTimeMatrix.get(key).getDuration() / 4;
				}
				delay += locationData.get(i).getDeliveryTime();

				distBetween = distanceTimeMatrix.get(key).getDuration() + delay;

				if (distBetween < minDist) {
					choosenID = i;
					minDist = distBetween;
				}
			}
		}

		return choosenID;
	}

	static public ArrayList<Integer> solve(ArrayList<LocationData> locationData,
			Map<String, DistanceData> distanceTimeMatrix, List<VehicleData> vehicleData, double startTime) {

		ArrayList<Integer> solution = new ArrayList<>();

		ArrayList<Integer> usedVehicles = new ArrayList<>();

		for (int i = 0; i < vehicleData.size(); ++i) {
			usedVehicles.add(0);
		}

		double currentTime = startTime;
		double currentWeight = 0;
		double currentVolume = 0;
		int currentLocation = 0;
		int newLocation = 0;
		String key;

		solution.add(0);

		while (hasFreeVehicle(vehicleData, usedVehicles)) {

			newLocation = findClosestLocationTo(currentLocation, currentTime, locationData, distanceTimeMatrix,
					vehicleData, solution, currentVolume, currentVolume, usedVehicles);

			if (newLocation == 0) {
				useOneVehicle(vehicleData, currentWeight, currentVolume, usedVehicles);
				currentTime = startTime;
				currentWeight = 0;
				currentVolume = 0;
				solution.add(0);
			} else {
				key = getKey(currentLocation, newLocation, locationData);

				double delay = 0;

				if (currentTime + distanceTimeMatrix.get(key).getDuration() < locationData.get(newLocation).getWindowStart()) {
					delay = locationData.get(newLocation).getWindowStart() - currentTime
							- distanceTimeMatrix.get(key).getDuration();
				}

				delay += locationData.get(newLocation).getDeliveryTime();

				currentTime += delay + distanceTimeMatrix.get(key).getDuration();
				currentWeight += locationData.get(newLocation).getCargoWeight();
				currentVolume += locationData.get(newLocation).getCargoVolume();
			}

		}

		return solution;

	}

	private static boolean hasFreeVehicle(List<VehicleData> vehicleData,ArrayList<Integer> usedVehicles ) {

		for ( int i = 0; i < vehicleData.size(); ++i ) {
			if ( usedVehicles.get(i) < vehicleData.get(i).getNumberOfVehicles()) {
				return true;
			}
		}
		return false;
	}

	private static boolean vehicleCanCarry(int i, double weight, double volume, List<VehicleData> vehicleData) {

		double vehicleCapacity = vehicleData.get(i).getWeightCap();
		double vehicleVolume = vehicleData.get(i).getVolumeCap();

		return (weight <= vehicleCapacity && volume <= vehicleVolume);
	}

	private static boolean existsFreeVehicleOfType(int i, List<VehicleData> vehicleData, ArrayList<Integer> usedVehicles) {

		int startNumOfvehicles = vehicleData.get(i).getNumberOfVehicles();

		return (usedVehicles.get(i) < startNumOfvehicles);
	}

	private static void useOneVehicle(List<VehicleData> vehicleData, double currentCapacity, double currentVolume,
			ArrayList<Integer> usedVehicles) {

		double minWeight = Double.MAX_VALUE;
		int typeOfVehicle = 0;

		for (int i = 0; i < vehicleData.size(); ++i) {
			if (vehicleCanCarry(i, currentCapacity, currentVolume, vehicleData)
					&& existsFreeVehicleOfType(i, vehicleData, usedVehicles)) {
				if (vehicleData.get(i).getWeightCap() < minWeight) {
					typeOfVehicle = i;
					minWeight = vehicleData.get(i).getWeightCap();
				}
			}
		}
		usedVehicles.set(typeOfVehicle, usedVehicles.get(typeOfVehicle) + 1);
	}

	private static boolean transitionDoesntOverflow(int nextLocation, double currentCapacity, double currentVolume,
			ArrayList<LocationData> locationData, List<VehicleData> vehicleData, ArrayList<Integer> usedVehicles) {

		double potentiallyCariedWeight = currentCapacity + locationData.get(nextLocation).getCargoWeight();
		double potentiallyCariedVolume = currentVolume + locationData.get(nextLocation).getCargoVolume();

		for (int k = 0; k < vehicleData.size(); ++k) {
			if (vehicleCanCarry(k, potentiallyCariedWeight, potentiallyCariedVolume, vehicleData)
					&& existsFreeVehicleOfType(k, vehicleData, usedVehicles)) {
				return true;
			}
		}

		return false;
	}

}
